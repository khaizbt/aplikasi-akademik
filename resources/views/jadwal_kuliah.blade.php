@extends('layouts.mahasiswa')
@section('title','Jadwal Kuliah')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">

                    @include('alert')

                    <div class="col-md-12">


                                <div id="list"></div>
                        </div>
                        <a href="/jadwal_kuliah/pdf" class="btn btn-danger">Cetak Jadwal Kuliah</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {

    tampil_kelas();

});
</script>
<script>
function tampil_kelas(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.get("/jadwal_kuliah/json",
    {
      _token    :   CSRF_TOKEN
    },
    function(data, status){
        $("#list").html(data);
  });
}
</script>
@endpush
