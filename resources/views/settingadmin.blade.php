@extends('layouts.global')
@section('title','Setting Profil Admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">



                <div class="card-body">

                    @include('alert')
                    @include('validation_error')

                    {{ Form::model($settingadmin,['url'=>'settingadmin','method'=>'put'])}}

                        @csrf
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Email</label>
                            <div class="col-md-5">
                                {{ Form::text('username',null,['class'=>'form-control','placeholder'=>'Username', 'readonly'=>''])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Nama Admin</label>
                            <div class="col-md-5">
                                {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Nama User'])}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Email</label>
                            <div class="col-md-5">
                                {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])}}
                            </div>
                        </div>



                        <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right">Password</label>
                        <div class="col-md-5">
                            {{ Form::password('password',['class'=>'form-control','placeholder'=>'Password'])}}
                        </div>
                    </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">

                                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                                <a href="/krs" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>

        </div>
    </div>
</div>
@endsection
