@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card bg-white border">
              <div class="card bg-light text-dark">
    <div class="card-body">Login Mahasiswa</div>
  </div>
  @include('danger')
                {{-- <div class="card-header bg-transparent border-0">{{ __('Login') }}</div> --}}

                <div class="card-body">
                    <form method="POST" action="/mahasiswa/login" aria-label="{{ __('Login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="email" class="col-sm-12 col-form-label pl-0">{{ __('NIM') }}</label>
                                <br>
                                <input id="nim" type="nim" class="form-control{{ $errors->has('nim') ? ' is-invalid' : '' }}" name="nim" value="{{ old('nim') }}" required autofocus>

                                @if ($errors->has('nim'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nim') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-md-12">
                                <label for="password" class="col-md-4 col-form-label text-md-left pl-0">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn-lg btn-block btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <br><br>
                                <div class="form-group row mb-0">


                            <div class="col-md-12">
                                <a href="/dosen/login" class="btn btn-primary btn-success btn-block " role="button" aria-pressed="true">Login Dosen</a>
                                <br>
                                <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <a href="/login" class="btn btn-primary btn-info btn-block " role="button" aria-pressed="true">Login Admin</a>
                                <br>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

</div>
@endsection
