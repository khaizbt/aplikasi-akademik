@if (session('danger'))


<div class="alert alert-danger" role="alert">
    <b>Peringatan</b> : {{ session('danger') }}
</div>
@endif
