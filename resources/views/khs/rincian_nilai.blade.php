@extends('layouts.mahasiswa')
@section('title','Data Ruangan')
@section('content')

<div class="container box">
   <h3 align="center">Ajax Dynamic Dependent Dropdown in Laravel</h3><br />
   <div class="form-group">
     <select id="country" name="category_id" class="form-control" style="width:350px" >
                 <option value="" selected disabled>Select</option>
     @foreach($nilai_list as $row)
     <option value="{{ $row->semester}}">{{ $row->semester }}</option>
     @endforeach

    </select>
   </div>
   <br />
   <div class="form-group">
                <label for="title">Pilih Matakuliah:</label>
                <select name="state" id="state" class="form-control" style="width:350px">
                </select>
            </div>

  </div>
@endsection
  @push('script')
  <script type="text/javascript">
  $(document).ready(function(){
    $('#country').change(function(){
    var CountryID = $(this).val();
    if(CountryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?semester="+CountryID,
           success:function(res){
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?state_id="+stateID,
           success:function(res){
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }

   });
</script>

@endpush
