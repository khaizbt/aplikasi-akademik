@extends('layouts.mahasiswa')
@section('title','Kartu Hasil Studi')
@section('content')

<form id="form1" name="form1" onsubmit="return false">
  @include('alert')


  <div class="form-group row">

      <div class="col-md-5">
        {{ Form::select('kategori',['1'=>'Semester 1','2'=>'Semester 2','3'=>'Semester 3','4'=>'Semester 4','5'=>'Semester 5','6'=>'Semester 6','7'=>'Semester 7','8'=>'Semester 8'],$semester_terpilih,['class'=>'form-control','id'=>'kategori','onclick'=>'tampilkan()'])}}
</div>
</div>
<div class="row">
&nbsp;&nbsp;&nbsp;<a href="khs_semester?semester=1" id="abc" class="btn btn-info">Tampil Data</a>&nbsp;
    <a href="download?semester=1" id="cba" class="btn btn-primary">Download Data</a>
</div>
</form>




<br><br>

<div class="col-md-8">
  @php
  $totalSKS=0;
  $totalMutu=0;
  @endphp

    <table class="table table-bordered">
        <tr>

        <th width="100">KODE MK</th>
        <th width="350">NAMA MK</th>
        <th width="50">SEMESTER</th>
        <th width="50">SKS</th>
          <th width="50">GRADE</th>
          <th width="50">BOBOT</th>


        </tr>
        <tbody id="khs">
        @foreach($mahasiswa as $row)
        @php


        $nilai = hitung_nilai($row->id);
        $grade = hitung_grade($nilai);
        $mutu  = hitung_mutu($grade);
        @endphp
        <tr>

            <td>{{ $row->kode_mk }}</td>
            <td>{{ $row->nama_mk }}</td>
            <td>{{ $row->semester }}</td>
            <td>{{ $row->jml_sks}}</td>
            <td class="grade">{{ $grade }}</td>
            <td>{{ $mutu*$row->jml_sks }}</td>
        </tr>
        <?php
        $totalSKS=$totalSKS+$row->jml_sks;
        $totalMutu = $totalMutu+$mutu*$row->jml_sks;
        ?>


        @endforeach
      </tbody>
      <tr>
          <td colspan="3">Jumlah SKS</td>
          <td colspan="3">{{ $totalSKS}}</td>
      </tr>
      <tr>
          <td colspan="3">Jumlah Mutu</td>
          <td colspan="3">{{ $totalMutu }}</td>
      </tr>
      <tr>
          <?php
          if(empty($totalMutu) or empty($totalSKS))
              $ipk =0;
          else
              $ipk = $totalMutu/$totalSKS;
          ?>

          <td colspan="3">IPK</td>
          <td colspan="3">{{ $ipk }}</td>
      </tr>
  </table>
</div>
@endsection

@push('scripts')
<script>
function tampilkan(){
  var nama_kota=document.getElementById("form1").kategori.value;
  if (nama_kota=="1")
    {
        document.getElementById("cba").href="download?semester=1";
        document.getElementById("abc").href="khs_semester?semester=1";
    }
    else if (nama_kota=="2")
      {
        document.getElementById("cba").href="download?semester=2";
        document.getElementById("abc").href="khs_semester?semester=2";
      }
    else if (nama_kota=="3")
      {
        document.getElementById("cba").href="download?semester=3";
        document.getElementById("abc").href="khs_semester?semester=3";
      }
      else if (nama_kota=="4")
        {
          document.getElementById("cba").href="download?semester=4";
          document.getElementById("abc").href="khs_semester?semester=4";
        }
        else if (nama_kota=="5")
          {
            document.getElementById("cba").href="download?semester=4";
            document.getElementById("abc").href="khs_semester?semester=4";
          }
          else if (nama_kota=="6")
            {
              document.getElementById("cba").href="download?semester=6";
              document.getElementById("abc").href="khs_semester?semester=6";
            }
            else if (nama_kota=="7")
              {
                document.getElementById("cba").href="download?semester=7";
                document.getElementById("abc").href="khs_semester?semester=7";
              }
              else if (nama_kota=="8")
                {
                  document.getElementById("cba").href="download?semester=8";
                  document.getElementById("abc").href="khs_semester?semester=8";
                }
}
</script>

<script type="text/javascript">
        $(document).ready(function(){
            $('#kategori').on('change', function(e){
              var grade = $("#khs.grade").innerHTML;
                var id = e.target.value;
                $.get('/khs/khs_semester/'+id, function(data){
                    console.log(id);
                    console.log(data);
                    $('#khs').empty();
                      $.each(data, function(index, element){
                        $('#khs').append("<tr><td>"+element.kode_mk+"</td><td>"+element.nama_mk+"</td>"+
                        "<td>"+element.semester+"</td><td>"+element.jml_sks+"</td><td>"+element.grade+"</td></tr>");
                    });
                });
            });
        });
    </script>
@endpush
