@extends('layouts.global')
@section('title','Data Jurusan')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Modul Jurusan</div>

                <div class="card-body">

                    <a href="/jurusan/create" class="btn btn-danger">Input Data Baru</a>


                    @include('alert')


                    <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th width="100">Kode Jurusan</th>
                                    <th  width="200">Nama Jurusan</th>
                                    <th  width="230">Nama Kelompok</th>
                                    <th width="25">Jenjang</th>
                                    <th width="290">Kaprodi</th>
                                    <th width="180">Action</th>
                                </tr>
                            </thead>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/jurusan/json',
        columns: [
            { data: 'kode_jurusan', name: 'kode_jurusan' },
            { data: 'nama_jurusan', name: 'nama_jurusan' },
            { data: 'nama_kelompok', name: 'nama_kelompok' },
            { data: 'jenjang', name: 'jenjang' },
            { data: 'nama', name: 'nama' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@endpush
