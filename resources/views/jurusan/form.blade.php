<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Nama Jurusan</label>
        <div class="col-md-8">
            {{ Form::text('nama_jurusan',null,['class'=>'form-control','placeholder'=>'Nama jurusan'])}}
        </div>
</div>

<div class="form-group row">
    <label class="col-md-2 col-form-label text-md-right">Kelompok</label>
    <div class="col-md-8">
            {{ Form::select('kode_kelompok',$kelompok,null,['class'=>'form-control'])}}
    </div>
</div>

<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">No Jurusan</label>
        <div class="col-md-8">
            {{ Form::text('no_jurusan',null,['class'=>'form-control','placeholder'=>'No Jurusan'])}}
        </div>
</div>

<div class="form-group row">
    <label class="col-md-2 col-form-label text-md-right">Jenjang</label>
    <div class="col-md-8">
        {{ Form::select('jenjang',['s1'=>'S1'],null,['class'=>'form-control'])}}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-2 col-form-label text-md-right">Kaprodi</label>
    <div class="col-md-8">
            {{ Form::select('nidn',$dosen,null,['class'=>'form-control'])}}



    </div>
</div>
