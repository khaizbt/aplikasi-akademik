@extends('layouts.global')
@section('title','Tahun Akademik')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">


                    <a href="/tahunakademik/create" class="btn btn-danger"><i class="fas fa-plus-square"></i> Input Data Baru</a>
<br><br>

                    @include('alert')


                    <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th width="50">Kode</th>
                                    <th>Tahun akademik</th>
                                    <th>Periode Perkuliahan</th>
                                    <th>Periode Pengajuan</th>
                                    <th>Periode KRS</th>
                                    <th>Status</th>
                                    <th width="110">Action</th>
                                </tr>
                            </thead>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/tahunakademik/json',
        columns: [
            { data: 'kode_tahun_akademik', name: 'kode_tahun_akademik' },
            { data: 'tahun_akademik', name: 'tahun_akademik' },
            { data: 'periode_perkuliahan', name: 'periode_perkuliahan' },
            { data: 'periode_pengajuan', name: 'periode_pengajuan' },
            { data: 'periode_krs', name: 'periode_krs' },

            { data: 'status', name: 'status' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@endpush
