@extends('layouts.mahasiswa')
@section('title','Cetak KRS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@yield('title')</div>

                <div class="card-body">

                    @include('alert')

                    <a href="/krs/pdf" class="btn btn-danger">Cetak KHS PDF</a>
                    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th width="100">Kode MK</th>
                <th width="150">Matakuliah</th>
                <th width="150">Dosen</th>
                <th width="100">Kelas</th>
                <th width='50'>Semester</th>


            </tr>
        </thead>
    </table>
      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({

        processing: true,
        serverSide: true,
        ajax: '/krs/json',
        columns: [
            { data: 'kode_mk', name: 'kode_mk' },
            { data: 'nama_mk', name: 'nama_mk' },
            { data: 'nama', name: 'nama' },
            { data: 'nama_kelas', name: 'nama_kelas'},
            { data: 'semester', name: 'semester'},

        ]
    });
});
</script>
@endpush
