@extends('layouts.mahasiswa')
@section('title','Pengajuan Matakuliah')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        
            <div class="card">


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <div class="row">
                        <div class="col-md-6">
                            <h6>Daftar Matakuliah Kontrak</h6>
                            <div id="lista"></div>

                        </div>
                        <div class="col-md-6">
                                <h6>Daftar Matakuliah Yang Anda Pilih</h6>

                                <div id="list"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<script>
$(function() {
    tampil_matkul();
    tampil_pengajuan();




});


function tambah_pengajuan(kode_mk,semester,data){


      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.get("/krs/tambahPengajuan",
      {
        kode_mk : kode_mk,
        semester:semester,
        _token: CSRF_TOKEN
      },
      function(data, status){
        tampil_matkul();
        tampil_pengajuan();

    });

  }
    function tampil_matkul(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.get("/krs/daftarPengajuan",
        {
          _token    :   CSRF_TOKEN
        },
        function(data, status){
            $("#lista").html(data);
      });
    }

    function tampil_pengajuan(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.get("/krs/tampilPengajuan",
        {
          _token    :   CSRF_TOKEN
        },
        function(data, status){
            $("#list").html(data);
      });
    }

    function hapus_pengajuan(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.post("/krs/hapusPengajuan",
        {
          id : id,
          _token: CSRF_TOKEN
        },
        function(data, status){
          tampil_matkul();
          tampil_pengajuan();

      });

    }


</script>
@endpush
