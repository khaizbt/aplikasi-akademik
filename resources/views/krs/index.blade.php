@extends('layouts.mahasiswa')
@section('title','Kartu Rencana Studi')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <h6>Daftar KRS Semester Selanjutnya</h6>
                              <div id="listan"></div>
                        </div>
                        <div class="col-md-12">
                              <div id="lista"></div>
                        </div>

                    </div>
                    <div class="col-md-13">
                                <h6>Daftar KRS Yang Anda Pilih</h6>

                                <div id="list"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {

    tampil_kelas();

    tampil_krs();


});
</script>

<script>

    function tambah_krs(id,kode_mk,nidn,semester, kode_kelas){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.get("/krs/tambahKrs",
        {
          id : id,
          kode_mk : kode_mk,
          nidn:nidn,
          semester:semester,
          kode_kelas:kode_kelas,
          _token: CSRF_TOKEN
        },
        function(data, status){

          tampil_kelas();
          tampil_krs();
      });
    }

    function tampil_kelas(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.get("/krs/daftarMatakuliahKontrak",
        {
          _token    :   CSRF_TOKEN
        },
        function(data, status){
            $("#lista").html(data);
      });
    }



    function tampil_krs(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.get("/krs/tampilKrs",
        {
          _token    :   CSRF_TOKEN
        },
        function(data, status){
            $("#list").html(data);
      });
    }

    function hapus_krs(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.post("/krs/hapusKrs",
        {
          id : id,
          _token: CSRF_TOKEN
        },
        function(data, status){
          tampil_kelas();

          tampil_krs();
      });
    }


</script>
@endpush
