<a href="{{ $url_show }}" class="btn btn-secondary" title="{{ $model->nama_mahasiswa }}">DETAIL</a>

<a href="{{ $url_edit }}" class="btn btn-primary" title="{{ $model->nama_mahasiswa }}">UBAH</a>

<a href="{{ $url_destroy }}" class="btn btn-danger btn-delete" title="{{ $model->nama_mahasiswa }}">HAPUS</a>
