<a href="{{ $url_show }}" class="btn btn-secondary" title="{{ $model->nama }}">Detail</a>

<a href="{{ $url_edit }}" class="btn btn-primary" title="{{ $model->nama }}">EDIT</a>

<a href="{{ $url_destroy }}" class="btn btn-danger btn-delete" title="{{ $model->nama }}">Hapus</a>
