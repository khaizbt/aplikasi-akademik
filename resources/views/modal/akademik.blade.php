

<a href="{{ $url_edit }}" class="btn btn-primary btn-sm"  title="{{ $model->tahun_akademik }}"><i class="fas fa-pencil-alt"></i></a>

<a href="{{ $url_destroy }}" class="btn btn-danger btn-delete" title="{{ $model->tahun_akademik }}"><i class='fas fa-trash-alt'></i></a>
