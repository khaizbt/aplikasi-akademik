@extends('layouts.global')
@section('title','Validasi KRS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">


                    @include('validation_error')


@php
$totalSKS=0;
@endphp

<table class="table table-bordered">



    <tr><td width="270">NIM</td><td>{{ $mahasiswa->nim}}</td></tr>
    <tr><td>Nama Mahasiswa</td><td>{{ $mahasiswa->nama_mahasiswa}}</td></tr>
    <tr><td>Kelas Asal</td><td>{{ $mahasiswa->nama_kelas}}</td></tr>
    <tr><td>Dosen Wali</td><td>{{ $mahasiswa->nama}}</td></tr>
</table>

                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <tr>
                                    <th width="130">NIM</th>
                                    <th width="300">NAMA</th>
                                    <th width="400">MATAKULIAH DIAJUKAN</th>
                                    <th width="100">Semester</th>
                                    <th>SKS</th>


                                    @foreach($pengajuan as $row)
                                    <tr>
                                        <td>{{ $row->nim}}</td>
                                        <td>{{ $row->nama_mahasiswa }}</td>
                                        <td>{{ $row->nama_mk }}</td>
                                        <td>{{ $row->semester }}</td>
                                        <td>{{ $row->jml_sks }}</td>

                            <?php
                            $totalSKS=$totalSKS+$row->jml_sks;
                            ?>

                                    <tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="3">Jumlah SKS</td>
                                        <td colspan="3">{{ $totalSKS}}</td>
                                    </tr>
                                </table>
                                <div class="col-md-3">
                                  <?php foreach ($pengajuan as $key => $value): ?>

                                  <?php endforeach; ?>
                                  <form action="/validasikrs/{{$value->nim}}/selesai">
                                            <a href="/validasikrs/{{$value->nim}}/selesai" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i>VALIDASI KRS</a>
                                        </form>

                                </div>


                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
