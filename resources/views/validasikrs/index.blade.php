@extends('layouts.global')
@section('title','Validasi KRS')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">



                    @include('alert')

                    <div class="row">

                        <div class="col-md-17">
                                <table class="table table-bordered" id="users-table">
                                        <thead>
                                            <tr>
                                                <th width>NIM</th>
                                                <th width="400">Nama Mahasiswa</th>

                                                <th width="200">Kelas</th>
                                                <th width>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/validasikrs/mahasiswa_json',
        columns: [
            { data: 'nim', name: 'nim' },
            { data: 'nama_mahasiswa', name: 'nama_mahasiswa' },

            { data: 'nama_kelas', name: 'nama_kelas' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@endpush
