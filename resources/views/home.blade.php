@extends('layouts.global')
@section('title','Home')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Selamat Datang <b>{{Auth::user()->name}}</b> <br><br>
                    Anda Bertidak sebagai <b>Admin</b>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
