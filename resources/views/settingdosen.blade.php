@extends('layouts.dosen')
@section('title','Setting Profil Dosen')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jika ada kesalahan penulisan nama, harap hubungi admin</div>

                <div class="card-body">

                    @include('alert')
                    @include('validation_error')

                    {{ Form::model($settingdosen,['url'=>'settingdosen','method'=>'put'])}}

                        @csrf

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Nama Dosen</label>
                            <div class="col-md-5">
                                {{ Form::text('nama',null,['class'=>'form-control','placeholder'=>'Nama Dosen','readonly'=>''])}}
                            </div>
                        </div>
                        <div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Tempat dan Tanggal Lahir</label>
        <div class="col-md-4">
            {{ Form::text('tempat_lahir',null,['class'=>'form-control','placeholder'=>'Tempat Lahir'])}}
        </div>
        <div class="col-md-4">
            {{ Form::date('tanggal_lahir',null,['class'=>'form-control','placeholder'=>'Tanggal Lahir'])}}
        </div>
</div>
<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Jenis Kelamin</label>
        <div class="col-md-8">
            {{ Form::select('jenis_kelamin',['L'=>'Laki-Laki','P'=>'Perempuan'],null,['class'=>'form-control'])}}
        </div>
</div>
<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Alamat</label>
        <div class="col-md-8">
            {{ Form::text('alamat',null,['class'=>'form-control','placeholder'=>'Alamat'])}}
        </div>
</div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Email & No HP</label>
                            <div class="col-md-3">
                                {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])}}
                            </div>
                            <div class="col-md-3">
                                {{ Form::number('no_hp',null,['class'=>'form-control','placeholder'=>'No HP'])}}
                            </div>
                        </div>



                        <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right">Password</label>
                        <div class="col-md-8">
                            {{ Form::password('password',['class'=>'form-control','placeholder'=>'Password'])}}
                        </div>
                    </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">

                                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                                <a href="/jadwal_mengajar" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
