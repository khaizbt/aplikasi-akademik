@extends('layouts.global')
@section('title','Data mahasiswa')
@section('content')
<div class="col-md-4">
    {{ Form::open(['url'=>'/mahasiswa/cetak','method'=>'GET'])}}

    @csrf

    <table class="table table-bordered">
        <tr>
            <td>Jurusan</td>
            <td>{{ Form::select('jurusan',$jurusan,$jurusan_terpilih,['class'=>'form-control'])}}</td></tr>
            <tr>
                <td>Semester</td>
                <td>{{ Form::select('tahun_akademik',$tahun_akademik,$tahun_terpilih,['class'=>'form-control'])}}</td>
            </tr>

        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i> Download dari Filter</button><br>
                <a href="/mahasiswa/semua" class="btn btn-info"><i class="fas fa-plus-square"></i> Download Semua Data</a>
            </td></tr>
    </table>

</form>
</div>

@endsection
