@extends('layouts.global')
@section('title','Upload Data mahasiswa')
@section('content')

<div class="container">
  @include('validation_error')
        <div class="row" style="padding-top: 30px">
            <div class="col-md-6">
                <a href="/mahasiswa/download" class="btn btn-tertiary">Download Template Upload Mahasiswa</a>
                <div class="card">

                    <div class="card-body">

                        <form action="{{ url('/mahasiswa/import') }}" method="post" enctype="multipart/form-data">
                            @csrf

                            @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-success">
                                    {{ session('error') }}
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="">File (.xls, .xlsx)</label>
                                <input type="file" class="form-control" name="file">
                                <p class="text-danger">{{ $errors->first('file') }}</p>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-sm">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
    @endsection
