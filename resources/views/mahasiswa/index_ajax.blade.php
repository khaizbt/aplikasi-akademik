@extends('layouts.global')
@section('title','Data Mahasiswa')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">

                    <a href="/mahasiswa/create" class="btn btn-danger">Input Data Baru</a>
                    <a href="/mahasiswa/import" class="btn btn-success"><i class="far fa-file-excel"></i> Upload Data Dari Excel</a>

                    <a href="/mahasiswa/pdf" class="btn btn-primary">Download Data</a>
                    @include('alert')

                    <div class="row">

                        <div class="col-md-12">
                            <table class="table table-bordered" id="users-table">
                                <thead>
                                    <tr>
                                        <th width="100">NIM</th>
                                        <th>Nama</th>
                                        <th>Jurusan</th>
                                        <th width="250">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
        $(document).ready(function() {
            tampil_data();
          });
</script>

<script>

    function tampil_data()
    {
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/mahasiswa/json',
                columns: [
                    { data: 'nim', name: 'nim' },
                    { data: 'nama_mahasiswa', name: 'nama_mahasiswa' },
                    { data: 'nama_jurusan', name: 'nama_jurusan' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    }

    function ubah_jurusan()
    {
        var jurusan = $(".jurusan").val();
        alert(jurusan);
        //tampil_data();
        $.ajax({
            type: "GET",
            url: '/mahasiswa/json',
            data: {jurusan: jurusan},
            success: function( msg ) {
                //$("#ajaxResponse").append("<div>"+msg+"</div>");
                alert('ok');
            }
        });


        $('#users-table').DataTable().ajax.reload();
    }
</script>
@endpush
