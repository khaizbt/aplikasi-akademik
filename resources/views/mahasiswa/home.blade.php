@extends('layouts.mahasiswa')
@section('title','Home')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Selamat Datang <b>{{ Auth::guard('mahasiswa')->user()->nama_mahasiswa }}</b> <br>
                    NIM <b>{{ Auth::guard('mahasiswa')->user()->nim }}</b>


                    <br><br>
                    <b>INFO</b>
                      <ul>
                        <li>Pengajuan Mata Kuliah Bisa Dilakukan Pada {{$tanggal_awal_pengajuan}} sampai {{$tanggal_akhir_pengajuan}}


                        <li>KRS Bisa Dilakukan Pada {{$tanggal_awal_krs}} sampai {{$tanggal_akhir_krs}}</li>

                        
                          </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
