@extends('layouts.global')

@section('title') Detail Mahasiswa @endsection

@section('content')
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <b>NIM:</b> <br/>
        {{$mahasiswa->nim}}<br>
        <b>Name:</b> <br/>
        {{$mahasiswa->nama_mahasiswa}}
        <br>
        <b>Jurusan:</b> <br/>
        {{$jurusan->nama_jurusan}}
        <br>


        <br>
        <b>Tempat dan Tanggal Lahir</b><br>
        {{$mahasiswa->tempat_lahir}}, {{$tanggal_lahir}}
        <br>
        <b>Nama Orang Tua</b><br>
        {{$mahasiswa->nama_ayah}} dan {{$mahasiswa->nama_ibu}}
        <br>
        <b>Jenis Kelamin</b><br>
        {{$mahasiswa->jenis_kelamin=='L'?'Laki-Laki':'Perempuan'}}
        <br>
        <b>Email:</b><br>
        {{$mahasiswa->email}}

        <br>
        <b>No HP</b> <br>
        {{$mahasiswa->no_hp}}
        <br>
        <b>Dosen Wali</b> <br>
        {{$jurusan->nama}}
        <br>
        <b>Kelas</b> <br>
        {{$jurusan->nama_kelas}}
        <br>
        <b>Alamat </b> <br>
        {{$mahasiswa->alamat}}



        <br><br><br>

        <a href="/mahasiswa/{{$mahasiswa->nim}}/edit" class="btn btn-primary">Edit Data Mahasiswa</a> <a href="/mahasiswa" class="btn btn-secondary">Kembali</a>

      </div>
    </div>
  </div>
@endsection
