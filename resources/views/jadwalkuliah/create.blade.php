@extends('layouts.global')

@section('title','Input Jadwal Kuliah')
@section('content')
<div class="container">
    <div class="row justify-content-center">

                <div class="card-body">
                    @include('danger')
                    @include('validation_error')

                    {{ Form::open(['url'=>'jadwalkuliah'])}}

                        @csrf

                        <div class="form-group row">
                        <label class="col-md-2 col-form-label text-md-right" for="matakuliah">Matakuliah</label><br>
                        <div class="col-md-8">
                        <select name="kode_mk" mutiple id="kode_mk" class="form-control"></select>
                        </div>
                                <br><br/>
                                </div>
                                <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right" for="matakuliah">Dosen</label><br>
                                <div class="col-md-8">
                                <select name="nidn" mutiple id="nidn" class="form-control"></select>
                                </div>
                                        <br><br/>
                                        </div>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">Jurusan</label>
                                    <div class="col-md-8">
                                            {{ Form::select('kode_jurusan',$jurusan,null,['class'=>'form-control'])}}
                                            {{ Form::hidden('kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)}}

                                    </div>
                                </div>



                        @include('jadwalkuliah.form')

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">

                                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                                <a href="/jadwalkuliah?&jurusan=ifd3&semester=1" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@include('jadwalkuliah.js')
