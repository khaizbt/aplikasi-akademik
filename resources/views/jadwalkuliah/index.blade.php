@extends('layouts.global')
@section('title','Jadwal Kuliah')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div class="card bg-primary text-white">
    <div class="card-body">Data Jadwal</div>
  </div>

                <div class="card-body">

                    @include('alert')
                            <div class="col-md-4">
                                {{ Form::open(['url'=>'jadwalkuliah','method'=>'GET'])}}

                                @csrf
                                <td colspan="2">

                                        {{ Form::select('jurusan',$jurusan,$jurusan_terpilih,['class'=>'form-control'])}}


                                        {{ Form::select('semester',['1'=>'Semester 1','2'=>'Semester 2','3'=>'Semester 3','4'=>'Semester 4','5'=>'Semester 5','6'=>'Semester 6','7'=>'Semester 7','8'=>'Semester 8'],$semester_terpilih,['class'=>'form-control'])}}
                                      </td>
</div>
                                    <tr>
                                      <br/>
                                        &nbsp;&nbsp;<td colspan="2">
                                            <button type="submit" class="btn btn-danger"><i class="fas fa-plus-square"></i> Refresh Data</button>
                                            <a href="/jadwalkuliah/create" class="btn btn-danger"><i class="fas fa-plus-square"></i> Input Jadwal</a>
                                        </td></tr>
                                </table>

                            </form>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>

                                    <th width="75">HARI</th>
                                    <th width="170">JAM</th>
                                    <th width="300">MATAKULIAH</th>
                                    <th width="350">DOSEN</th>
                                    <th width="10">RUANG</th>
                                    <th>KELAS</th>

                                    <th width="170"></th></tr>
                                    @foreach($jadwal as $row)
                                    <tr>

                                        <td>{{ $row->hari }}</td>
                                        <td>{{ $row->jam }}</td>
                                        <td>{{ $row->nama_mk }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td>{{ $row->nama_ruangan }}</td>
                                        <td>{{ $row->nama_kelas}}</td>

                                        <td><a href="jadwalkuliah/{{ $row->id }}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                        <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{route('jadwalkuliah.destroy', $row->id)}}"><i class="fa fa-trash"></i></a></td>
                                    <tr>
                                    @endforeach


                                </table>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
