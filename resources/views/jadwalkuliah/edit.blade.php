@extends('layouts.global')
@section('title','Edit Data Jadwal Kuliah')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">
                    @include('alert')

                    {{ Form::model($jadwal_kuliah,['url'=>'jadwalkuliah/'.$jadwal_kuliah->id,'method'=>'PUT'])}}

                        @csrf

                        @foreach($matakuliah_terpilih as $row)

                        <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Matakuliah</label>
                                <div class="col-md-8">
                                        {{ $row->nama_mk }}

                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Jurusan</label>
                                <div class="col-md-8">
                                        {{ $row->nama_jurusan }}

                                </div>
                        </div>

                        @endforeach
                        <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Dosen</label>
                                <div class="col-md-8">
                                        {{ Form::select('nidn',$dosen,null,['class'=>'form-control'])}}
                                </div>
                        </div>

                        @include('jadwalkuliah.form')

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">

                                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                                <a href="/jadwalkuliah?&jurusan=ifd3&semester=1" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include('jadwalkuliah.js')
