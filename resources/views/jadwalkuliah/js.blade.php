@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$('#kode_mk').select2({ajax: { url: '/ajax/kuliah/search',
processResults: function(data){
return {
results: data.map(function(matakuliah){return {id: matakuliah.kode_mk, text:matakuliah.nama_mk} })
    }
  }
 }
});
</script>
<script>
$('#nidn').select2({ajax: { url: '/ajax/datadosen/search',
processResults: function(data){
return {
results: data.map(function(dosen){return {id: dosen.nidn, text:dosen.nama} })
    }
  }
 }
});
</script>
<script>
         $(document).ready(function (){
         validate();
         $('#remember').change(validate);
         });

         function validate() {
        if ($('#remember').is(':checked')
         ) {
          $('#jam').show();
          $('#ruang').show();
          $('#praktik').show();
 } else {
     $('#jam').hide();
     $('#ruang').hide();
     $('#praktik').hide();

 }
}

      </script>
@endsection
