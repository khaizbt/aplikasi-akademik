

<div class="form-group row">
    <label class="col-md-2 col-form-label text-md-right">Semester</label>
    <div class="col-md-8">
            {{ Form::select('semester',['1'=>'Semester 1','2'=>'Semester 2','3'=>'Semester 3','4'=>'Semester 4','5'=>'Semester 5','6'=>'Semester 6','7'=>'Semester 7','8'=>'Semester 8'],null,['class'=>'form-control'])}}
    </div>
</div>

<!-- <div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Matakuliah</label>
        <div class="col-md-8">
                {{ Form::select('kode_mk',$matakuliah,null,['class'=>'form-control'])}}
        </div>
</div> -->



<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Ruangan</label>
        <div class="col-md-8">
                {{ Form::select('kode_ruangan',$ruangan,null,['class'=>'form-control'])}}
        </div>
</div>

<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Hari Dan Jam</label>
        <div class="col-md-2">
                {{ Form::select('hari',$hari,null,['class'=>'form-control'])}}
        </div>
        <div class="col-md-3">
                {{ Form::select('jam',$jamkuliah,null,['class'=>'form-control'])}}
        </div>
</div>
<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Kelas</label>
        <div class="col-md-2">
                {{ Form::select('kode_kelas',$kelas,null,['class'=>'form-control'])}}
        </div>
</div>

<div class="form-group row">
    <div class="col-md-12">
        <div class="checkbox">
          <label class="col-md-2 col-form-label text-md-right"></label>
        <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}> <label for="remember">{{ __('Praktikum') }}</label>
        </div>
    </div>
</div>

<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right " id="praktik">Ruang Dan Jam Praktikum</label>
        <div class="col-md-2">
                {{ Form::select('ruang_praktik',$ruang_praktik,null,['class'=>'form-control','id'=>'ruang'])}}
        </div>
        <div class="col-md-3">
                {{ Form::select('jam_praktik',$jam_praktik,null,['class'=>'form-control','id'=>'jam'])}}
        </div>
</div>
