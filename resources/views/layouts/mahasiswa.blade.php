<!DOCTYPE html>
<!--[if IE 9]> <html class="ie9 no-js" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield("title")</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
  <link rel="stylesheet" href="{{asset('polished/polished.min.css')}}">
  <link rel="stylesheet" href="{{asset('polished/iconic/css/open-iconic-bootstrap.min.css')}}">
  <style>
    .grid-highlight {
      padding-top: 1rem;
      padding-bottom: 1rem;
      background-color: #5c6ac4;
      border: 1px solid #202e78;
      color: #fff;
    }

    hr {
      margin: 6rem 0;
    }

    hr+.display-3,
    hr+.display-2+.display-3 {
      margin-bottom: 2rem;
    }
  </style>
  <script type="text/javascript">
    document.documentElement.className = document.documentElement.className.replace('no-js', 'js') + (document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? ' svg' : ' no-svg');
  </script>
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>

    <nav class="navbar navbar-expand p-0">
     <a class="navbar-brand text-center col-xs-12 col-md-3 col-lg-2 mr-0" href="/home">Aplikasi Akademik STMIK Komputama Majenang</a>
      <button class="btn btn-link d-block d-md-none" data-toggle="collapse" data-target="#sidebar-nav" role="button" >
        <span class="oi oi-menu"></span>
      </button>

      &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
      &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
      <!-- <div class="border-dark bg-primary-darkest form-control d-none d-md-block w-50 ml-3 mr-2" type="text" placeholder="Search" aria-label="Search"> -->
      <div class="dropdown d-none d-md-block">
        @if(Auth::guard('mahasiswa')->user())
        <button class="btn btn-link btn-link-primary dropdown-toggle" id="navbar-dropdown" data-toggle="dropdown">

          {{ Auth::guard('mahasiswa')->user()->nama_mahasiswa }}
        </button>
        @endif
        <div class="dropdown-menu dropdown-menu-right" id="navbar-dropdown">
          <a href="/settingmahasiswa" class="dropdown-item">Setting Profil</a>

          <div class="dropdown-divider"></div>
          <li>
            <form action="{{route("logout")}}" method="POST">
              @csrf
              <button class="dropdown-item" style="cursor:pointer">Sign Out</button>
            </form>
          </li>
        </div>
      </div>
    </nav>

  <div class="container-fluid h-100 p-0">
    <div style="min-height: 100%" class="flex-row d-flex align-items-stretch m-0">
        <div class="polished-sidebar bg-light col-12 col-md-3 col-lg-2 p-0 collapse d-md-inline" id="sidebar-nav">

            <ul class="polished-sidebar-menu ml-0 pt-4 p-0 d-md-block">
              <input class="border-dark form-control d-block d-md-none mb-4" type="text" placeholder="Search" aria-label="Search" />
              <li><a href="/mahasiswa/home"><span class="oi oi-home"></span> Home</a></li>
              <li><a href="/jadwal_kuliah"><i class="far fa-calendar-alt"></i>&nbsp;&nbsp;&nbsp;&nbsp; Jadwal Kuliah</a></li>
              <div class="dropdown d-none d-sm-block">
  <button class="btn btn-link btn-link-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="fas fa-user-tie"></i>
    &nbsp;&nbsp;&nbsp; Nilai Mahasiswa </button>
  <div class="dropdown-menu">
    <li><a href="/khs/khs_semester"><i class="fas fa-user-graduate"></i>  KHS Semester</a></li>
    <li><a href="/khs/transkrip_nilai"><i class="fas fa-calculator"></i> Transkrip Nilai</a></li>

  </div>
</div>
<div class="dropdown d-none d-sm-block">
<button class="btn btn-link btn-link-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&nbsp;<i class="fas fa-table"></i>
&nbsp;&nbsp;&nbsp; Pengajuan </button>
<div class="dropdown-menu">
<li><a href="/krs/pengajuan"><i class="fas fa-user-graduate"></i>  Pengajuan Matakuliah</a></li>
<li><a href="/krs"><i class="fas fa-users"></i> Kartu Rencana Studi</a></li>
<li><a href="/krs/cetak"><i class="fas fa-users"></i> Cetak KRS</a></li>

</div>
</div>


              <div class="d-block d-md-none">
                  <div class="dropdown-divider"></div>
                  <li><a href="/settingmahasiswa"> Profile</a></li>
                  <li><a href="/khs/khs_semester"> KHS Semester</a></li>
                  <li><a href="/khs/transkrip_nilai"> Transkrip Nilai</a></li>

                  <li>
                    <form action="{{route("logout")}}" method="POST">
                      @csrf
                      <button class="dropdown-item" style="cursor:pointer">Sign Out</button>
                    </form>
                  </li>
              </div>
            </ul>

        </div>
        <div class="col-lg-10 col-md-9 p-4">
            <div class="row ">
              <div class="col-md-12 pl-3 pt-2">
                  <div class="pl-3">
                    <h3>@yield("title")</h3>
                    <br>
                  </div>
              </div>
            </div>

            @yield("content")


        </div>
      </div>
  </div>

  <script  src="https://code.jquery.com/jquery-3.3.1.min.js"  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  <script src="{{ asset('sweetalert2/sweetalert2.all.min.js') }}"></script>
  <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
  <script src="{{ asset('js/app.js') }}"></script>

  @stack('scripts')
  @yield('footer-scripts')
</body>

</html>
