@section('footer-scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$('#nidn').select2({ajax: { url: '/ajax/datadosen/search',
processResults: function(data){
return {
results: data.map(function(dosen){return {id: dosen.nidn, text:dosen.nama} })
    }
  }
 }
});
</script>
@endsection
