@extends('layouts.global')
@section('title','Data Kelas')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

                    <a href="/kelas/create" class="btn btn-success pull-right modal-show" style="margin-top: -8px;" title="Create User"><i class="fas fa-plus"></i> Input Data Baru</a>
                    <br><br>

                    @include('alert')


                    <table class="table table-bordered" id="users-table">

                            <thead>
                                <tr><b>
                                    <th width="100"><b>Kode Kelas<b></th>
                                    <th width='150'><b>Nama Kelas<b></th>
                                    <th width="275"><b>Dosen Pembimbing<b></th>
                                    <th width="15"><b>Action<b></th>
                                <b></tr>
                            </thead>

                        </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection



@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/kelas/json',
        columns: [
            { data: 'kode_kelas', name: 'kode_kelas' },
            { data: 'nama_kelas', name: 'nama_kelas' },
            { data: 'nama', name: 'nama' },
            { data: 'action', name: 'action' }
        ]

});
});



</script>




    @endpush
