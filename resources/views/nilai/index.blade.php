@extends('layouts.dosen')
@section('title','Input Nilai Mahasiswa')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Input Nilai Mahasiswa</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <tr><td width="270">Kode Matakuliah</td><td>{{ $jadwal->kode_mk}}</td></tr>
                        <tr><td>Nama Matakuliah</td><td>{{ $jadwal->nama_mk}}</td></tr>
                        <tr><td>Nama Dosen</td><td>{{ $jadwal->nama}}</td></tr>
                    </table>
                    @foreach($khs as $key)
                    <a href="/nilai/{{$key->id_jadwal}}/import" class="btn btn-primary">Upload Nilai</a>

                      <a href="/nilai/{{$key->id_jadwal}}/download" class="btn btn-tertiary">Download Template Nilai Mahasiswa</a>
                      @endforeach
                    <h3>Kontrak Kuliah</h3>
                    <table class="table table-bordered">
                        <tr><th>Kode MK</th><th>Nama MK</th><th>Kehadiran(%)</th><th>Tugas(%)</th><th>UTS(%)</th><th>UAS(%)</th></tr>
                        @foreach($kontrak as $row)
                        <tr>
                        <td>{{ $row->kode_mk}}</td>
                            <td>{{ $row->nama_mk}}</td>
                            <td><input id="k_kehadiran-{{ $row->id}}" onkeyup="simpan_kontrak('{{$row->id}}')" type="text" value="{{ $row->kontrak_kehadiran}}" onkeyup="calc('{{$row->id}}')"></td>
                            <td><input id="k_tugas-{{ $row->id}}" onkeyup="simpan_kontrak('{{$row->id}}')" type="text" value="{{ $row->kontrak_tugas}}" onkeyup="calc('{{$row->id}}')"></td>
                            <td><input id="k_uts-{{ $row->id}}" onkeyup="simpan_kontrak('{{$row->id}}')" type="text" value="{{ $row->kontrak_uts}}" onkeyup="calc('{{$row->id}}')"></td>
                            <td><input id="k_uas-{{ $row->id}}" onkeyup="simpan_kontrak('{{$row->id}}')" type="text" value="{{ $row->kontrak_uas}}" onkeyup="calc('{{$row->id}}')"></td>
                            <td><span id="result"></span></td>
                        </tr>
                        @endforeach
                    </table>

                    <h3>NIlai Kuliah</h3>
                    <table class="table table-bordered">
                        <tr><th>NIM</th><th>Nama Mahassiwa</th><th>Kehadiran</th><th>Tugas</th><th>UTS</th><th>UAS</th></tr>
                        @foreach($mahasiswa as $row)
                        <tr>
                            <td>{{ $row->nim}}</td>
                            <td>{{ $row->nama_mahasiswa}}</td>
                            <td><input id="kehadiran-{{ $row->id}}" onkeyup="simpan_nilai('{{$row->id}}')" type="text" value="{{ $row->nilai_kehadiran}}"></td>
                            <td><input id="tugas-{{ $row->id}}" onkeyup="simpan_nilai('{{$row->id}}')" type="text" value="{{ $row->nilai_tugas}}"></td>
                            <td><input id="uts-{{ $row->id}}" onkeyup="simpan_nilai('{{$row->id}}')" type="text" value="{{ $row->nilai_uts}}"></td>
                            <td><input id="uas-{{ $row->id}}" onkeyup="simpan_nilai('{{$row->id}}')" type="text" value="{{ $row->nilai_uas}}"></td>

                        </tr>

                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

    function simpan_nilai(id_khs)
    {
        var nilai_uas = $("#uas-"+id_khs).val();
        var nilai_uts = $("#uts-"+id_khs).val();
        var nilai_tugas = $("#tugas-"+id_khs).val();
        var nilai_kehadiran = $("#kehadiran-"+id_khs).val();


        console.log(nilai_uas);
        console.log(nilai_uts);
        console.log(nilai_tugas);
        console.log(nilai_kehadiran);

        let n_kehadiran = parseFloat(nilai_kehadiran);
        let n_tugas = parseFloat(nilai_tugas);
        let n_uts = parseFloat(nilai_uts);
        let n_uas = parseFloat(nilai_uas);

        if(n_kehadiran > 100){
          Swal.fire({
          type: 'error',
          title: 'Nilai Kehadiran maksimal 100!',
          text: 'Harap periksa kambali!',
        })
        }
        if(n_tugas > 100){
          Swal.fire({
          type: 'error',
          title: 'Nilai Tugas maksimal 100!',
          text: 'Harap periksa kambali!',
        })
        }
        if(n_uts > 100){
          Swal.fire({
          type: 'error',
          title: 'Nilai UTS maksimal 100!',
          text: 'Harap periksa kambali!',
        })
        }
        if(n_uas > 100){
          Swal.fire({
          type: 'error',
          title: 'Nilai UAS maksimal 100!',
          text: 'Harap periksa kambali!',
        })
        }



        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.post("/nilai/update_nilai/update",
        {
          id_khs : id_khs,
          nilai_uas : nilai_uas,
          nilai_uts:nilai_uts,
          nilai_tugas:nilai_tugas,
          nilai_kehadiran:nilai_kehadiran,
          _token: CSRF_TOKEN
        },
        function(data, status){
          //alert('sukses')
      });
    }
</script>

<script>

    function simpan_kontrak(id_kontrak)
    {
        var kontrak_uas = $("#k_uas-"+id_kontrak).val();
        var kontrak_uts = $("#k_uts-"+id_kontrak).val();
        var kontrak_tugas = $("#k_tugas-"+id_kontrak).val();
        var kontrak_kehadiran = $("#k_kehadiran-"+id_kontrak).val();
        console.log(kontrak_kehadiran);
        console.log(kontrak_tugas);
        console.log(kontrak_uts);
        console.log(kontrak_uas);

let Total_kontrak = parseFloat(kontrak_uas) + parseFloat(kontrak_uts) + parseFloat(kontrak_tugas) + parseFloat(kontrak_kehadiran);

if (Total_kontrak >100){
  Swal.fire({

  type: 'error',
  title: 'Kontrak Kuliah Maksimal 100',
  text: 'Harap sesuaikan jumlah antar kolom!',
})
}

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.post("/kontrak/update_kontrak/update",
        {
          id_kontrak : id_kontrak,
          kontrak_uas : kontrak_uas,
          kontrak_uts:kontrak_uts,
          kontrak_tugas:kontrak_tugas,
          kontrak_kehadiran:kontrak_kehadiran,
          _token: CSRF_TOKEN
        },
        function(data, status){
          //alert('sukses')
      });
    }

    </script>
@endpush
