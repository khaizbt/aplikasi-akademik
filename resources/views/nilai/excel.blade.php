<table border="1">
    <tr>
        <th>NIDN</th>
        <th>Nama Dosen</th>
        <th>No HP</th>
        <th>Email</th>
        <th><b>Alamat</b></th>
    </tr>
    @foreach($khs as $row)
    <tr>
        <td>{{ $row->kode_mk }}</td>
        <td>{{ $row->nidn }}</td>
        <td>{{ $row->jam }}</td>
        <td>{{ $row->hari }}</td>
        <td>{{ $row->kode_kelas }}</td>
    </tr>
    @endforeach
</table>
