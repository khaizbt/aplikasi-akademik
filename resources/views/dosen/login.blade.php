@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card bg-white border">
              <div class="card bg-primary text-white">
    <div class="card-body">Login Dosen</div>
  </div>
@include('danger')

                {{-- <div class="card-header bg-transparent border-0">{{ __('Login') }}</div> --}}

                <div class="card-body">
                    <form method="POST" action="/dosen/login" aria-label="{{ __('Login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="email" class="col-sm-12 col-form-label pl-0">{{ __('NIDN') }}</label>
                                <br>
                                <input id="nidn" type="nidn" class="form-control{{ $errors->has('nidn') ? ' is-invalid' : '' }}" name="nidn" value="{{ old('nidn') }}" required autofocus>

                                @if ($errors->has('nidn'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-md-12">
                                <label for="password" class="col-md-4 col-form-label text-md-left pl-0">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn-block btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                                <br>
                                <div class="form-group row mb-0">



                    </form>
                </div>
            </div>
        </div>

</div>
@endsection
