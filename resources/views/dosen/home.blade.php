@extends('layouts.dosen')
@section('title','Home')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Selamat Datang <b>{{Auth::user('dosen')->nama}}</b> <br><br>
                    NIDN <b>{{Auth::user('dosen')->nidn}}</b>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
