@extends('layouts.dosen')
@section('title','Jadwal Mengajar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Upload Nilai Hanya Bisa Dilakukan Pada {{$tanggal_awal_kuliah}} sampai {{$tanggal_akhir_kuliah}}</div>

                <div class="card-body">

                    @include('alert')

                    <a href="/jadwal_mengajar/download" class="btn btn-danger">Cetak Jadwal Mengajar</a>
                    <br>
                    <br>
                    <div class="col-md-12">


                                <div id="list"></div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {

    tampil_kelas();

});
</script>
<script>
function tampil_kelas(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.get("/jadwal_mengajar/json",
    {
      _token    :   CSRF_TOKEN
    },
    function(data, status){
        $("#list").html(data);
  });
}
</script>
@endpush
