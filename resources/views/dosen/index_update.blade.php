@extends('layouts.global')
@section('title','Data Dosen')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">


                    <div class="row">
      <div class="col-md-12 text-left">
          <a href="/dosen/create" class="btn btn-primary"><i class="far fa-plus-square"></i> Create user</a>
          <a href="/dosen/excel" class="btn btn-info text-white"><i class="far fa-file-excel"></i> Export Excel</a>
      </div>
    </div>

                <br><br>
                    @include('alert')

                    <div class="row">
                        
                        <div class="col-md-17">
                                <table class="table table-bordered" id="users-table">
                                        <thead>
                                            <tr>
                                                <th width="150"><b>NIDN<b></th>
                                                <th width="400"><b>Nama Dosen<b></th>
                                                <th width="200"><b>Nomor Telepon<b></th>
                                                <th width="250"><b>Action<b></th>
                                            </tr>
                                        </thead>
                                    </table>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/dosen/json',
        columns: [
            { data: 'nidn', name: 'nidn' },
            { data: 'nama', name: 'nama' },
            { data: 'no_hp', name: 'no_hp' },

            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@endpush
