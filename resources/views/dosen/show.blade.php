@extends('layouts.global')

@section('title') Detail Dosen @endsection

@section('content')
  <div class="col-md-8">
    <div class="card">
      <div class="card-body">
        <b>NIDN:</b> <br/>
        {{$dosen->nidn}}
        <br>
        <b>Name:</b> <br/>
        {{$dosen->nama}}
        <br>

        <br>
        <b>Tempat dan Tanggal Lahir</b><br>
        {{$dosen->tempat_lahir}}, {{$tanggal_lahir}}

        <br>
        <b>Email:</b><br>
        {{$dosen->email}}

        <br>
        <b>No HP</b> <br>
        {{$dosen->no_hp}}

        <br>
        <b>Alamat </b> <br>
        {{$dosen->alamat}}



        <br><br><br>

        <a href="/dosen/{{$dosen->nidn}}/edit" class="btn btn-primary">Edit Data Dosen</a> <a href="/mahasiswa" class="btn btn-secondary">Kembali</a>

      </div>
    </div>
  </div>
@endsection
