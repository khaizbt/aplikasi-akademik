

<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Nama Dosen</label>
        <div class="col-md-8">
            {{ Form::text('nama',null,['class'=>'form-control','placeholder'=>'Nama Dosen'])}}
        </div>
</div>
<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Tempat dan Tanggal Lahir</label>
        <div class="col-md-4">
            {{ Form::text('tempat_lahir',null,['class'=>'form-control','placeholder'=>'Tempat Lahir'])}}
        </div>
        <div class="col-md-4">
            {{ Form::date('tanggal_lahir',null,['class'=>'form-control','placeholder'=>'Tanggal Lahir'])}}
        </div>
</div>
<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Jenis Kelamin</label>
        <div class="col-md-8">
            {{ Form::select('jenis_kelamin',['L'=>'Laki-Laki','P'=>'Perempuan'],null,['class'=>'form-control'])}}
        </div>
</div>
<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Alamat</label>
        <div class="col-md-8">
            {{ Form::text('alamat',null,['class'=>'form-control','placeholder'=>'Alamat'])}}
        </div>
</div>


<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">No HP</label>
        <div class="col-md-4">
            {{ Form::number('no_hp',null,['class'=>'form-control','placeholder'=>'No Hp'])}}
        </div>
    </div>


<div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">Email</label>
        <div class="col-md-4">
            {{ Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])}}
        </div>
</div>
