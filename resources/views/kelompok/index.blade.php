@extends('layouts.global')
@section('title','Kelompok')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Modul Kelompok</div>

                <div class="card-body">

                    <a href="/kelompok/create" class="btn btn-danger">Input Data Baru</a>


                    @include('alert')


                    <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th width="100">Kode Kelompok</th>
                                    <th>Nama Kelompok</th>
                                    <th width="190">Action</th>
                                </tr>
                            </thead>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/kelompok/json',
        columns: [
            { data: 'kode_kelompok', name: 'kode_kelompok' },
            { data: 'nama_kelompok', name: 'nama_kelompok' },
            { data: 'action', name: 'action' }
        ]
    });
});
</script>
@endpush
