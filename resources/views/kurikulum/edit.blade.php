@extends('layouts.global')
@section('title','Edit Data jurusan')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Data Jurusan</div>

                <div class="card-body">
                    @include('validation_error')

                    {{ Form::model($kurikulum,['url'=>'kurikulum/'.$kurikulum->id,'method'=>'PUT'])}}

                        @csrf

                        @foreach($matkul as $row)

                        <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Matakuliah</label>
                                <div class="col-md-8">
                                        {{ $row->nama_mk }}

                                </div>
                        </div>
                        @endforeach

                        @include('kurikulum.form')

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">

                                {{ Form::submit('Simpan Data',['class'=>'btn btn-primary'])}}
                                <a href="/jurusan" class="btn btn-primary">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
