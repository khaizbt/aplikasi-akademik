@extends('layouts.global')
@section('title','Data Kurikulum')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <div class="card-body">
                    @include('alert')

                    <div class="row">
                        <div class="col-md-4">
                            {{ Form::open(['url'=>'kurikulum','method'=>'GET'])}}
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Jurusan</td>
                                        <td>{{ Form::select('jurusan',$jurusan,$jurusan_terpilih,['class'=>'form-control'])}}</td>
                                        <tr>
                                    <tr>

                                        <td colspan="2">
                                            <button type="submit" class="btn btn-danger"><i class="fas fa-sync-alt"></i> Refresh</button>
                                            <a href="/kurikulum/create" class="btn btn-danger">Input Data Baru</a></td>
                                    </tr>
                                </table>
                            {{ Form::close()}}
                        </div>
                        <div class="col-md-8">
                                <table class="table table-bordered" id="users-table">
                                        <thead>
                                            <tr>
                                                <th width="100">Kode MK</th>
                                                <th width="200">Nama Matakuliah</th>
                                                <th width="10">SKS</th>
                                                <th width="40">Semester</th>
                                                <th width="100">Action</th>
                                            </tr>

                                            @foreach($kurikulum as $row)
                                                <tr>
                                                    <td>{{ $row->kode_mk }}</td>
                                                    <td>{{ $row->nama_mk }}</td>
                                                    <td align="center">{{ $row->jml_sks }}</td>
                                                    <td align="center">{{ $row->semester }}</td>
                                                    <td><a href="kurikulum/{{ $row->id }}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                                    <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{route('kurikulum.destroy', $row->id)}}"><i class="fa fa-trash"></i></td>
                                                </tr>
                                            @endforeach
                                        </thead>
                                    </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
