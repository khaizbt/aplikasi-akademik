<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Auth;
use App\KHS;
use Fpdf;
use DB;

class JadwalMahasiswaController extends Controller
{
    function jadwal_kuliah(){

        return view('jadwal_kuliah');
    }

    function jadwal_kuliah_json(){

      $result = '<table class="table table-bordered">
              <tr><th width="230">Nama Matakuliah</th><th width="250">Nama Dosen</th><th width="80">hari</th><th width="130">Teori(Jam/Ruang)</th><th width="130">Praktikum(Jam/Ruang)</th><th width="130">Kelas</th><tr>';

        $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
        $nim = Auth::guard('mahasiswa')->user()->nim;
        $khs = \DB::table('khs')
          ->where('kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
          ->where('nim',$nim)
          ->get();



          foreach($khs as $key){
            $values[] = $key->id_jadwal;
          }



      if(!empty($values)){


      $jadwal =  DB::table("jadwal_kuliah")
                // ->join('khs','jadwal_kuliah.id','=','khs.id')
                ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                ->where('jadwal_kuliah.id',$values)
                ->select('jadwal_kuliah.id','jadwal_kuliah.kode_mk','jadwal_kuliah.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','ruangan.nama_ruangan','jam_kuliah.jam','kelas.nama_kelas')
                ->get();




              }
              else{
                $jadwal =  DB::table("jadwal_kuliah")
                          ->join('khs','jadwal_kuliah.kode_mk','=','khs.kode_mk')
                          ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                          ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                          ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                          ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                          ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                          ->where('khs.nim',$nim)
                          ->where('khs.kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
                          ->select('jadwal_kuliah.id','khs.kode_tahun_akademik','khs.kode_mk','khs.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','khs.nim','ruangan.nama_ruangan','jam_kuliah.jam','kelas.nama_kelas')
                          ->get();
              }


                             foreach($jadwal as $word)
                             {

                               $praktikum = DB::table('jadwal_kuliah')
                                           ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                                           ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                                           ->where('jadwal_kuliah.id',$word->id)
                                           ->get()->toArray();



                                           if(!empty($praktikum)){

                                           $result .= '<tr>
                                                       <td>'.$word->nama_mk.'</td>
                                                       <td>'.$word->nama.'</td>
                                                       <td>'.$word->hari.'</td>
                                                       <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                       <td>'.$praktikum[0]->jam.'/'.$praktikum[0]->nama_ruangan.'</td>
                                                       <td>'.$word->nama_kelas.'</td>

                                                       </tr>';
                                                     }
                                                     else{
                                                       $result .= '<tr>
                                                                   <td>'.$word->nama_mk.'</td>
                                                                   <td>'.$word->nama.'</td>
                                                                   <td>'.$word->hari.'</td>
                                                                   <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                                   <td>'.'-'.'</td>
                                                                   <td>'.$word->nama_kelas.'</td>

                                                                   </tr>';

                                                     }
                             }


             $result .='</table>';


            return $result;
    }

    function DownloadJadwal(){
      $nim = Auth::guard('mahasiswa')->user()->nim;
      $nama = Auth::guard('mahasiswa')->user()->nama_mahasiswa;
      $mahasiswa = \DB::table('mahasiswa')
                  // ->join('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                  ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                  ->leftJoin('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                  ->leftJoin('dosen','kelas.nidn','=','dosen.nidn')
                  ->where('mahasiswa.nim',$nim)
                  ->first();




      $jurusan = \DB::table('mahasiswa')
                  ->leftJoin('jurusan','mahasiswa.kode_jurusan','=','jurusan.kode_jurusan')
                  ->leftJoin('dosen','jurusan.nidn','=','dosen.nidn')
                  ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                  ->where('mahasiswa.nim',$nim)
                  ->first();

      $tahunaktif = \DB::table('tahun_akademik')
                ->where('tahun_akademik.status','y')->first();

      $khs = \DB::table('khs')
        ->where('kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
        ->where('nim',$nim)
        ->get();


      $setting    = \DB::table('setting')->where('id',1)->first();

      $jadwalkuliah = \DB::table('jadwal_kuliah')
                      ->join('khs','jadwal_kuliah.id','=','khs.id_jadwal')
                      ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                      ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                      ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                      ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                      ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                      ->where('nim',$nim)
                      ->select('jadwal_kuliah.id','jadwal_kuliah.hari','jam_kuliah.jam','matakuliah.nama_mk','dosen.nama','kelas.nama_kelas','ruangan.nama_ruangan')
                      ->get();

      // $khs = \DB::table('khs')
      //         ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
      //         ->join('dosen','dosen.nidn','=','khs.nidn')
      //         ->where('nim',$nim)
      //         ->get();



      $info = \DB::table('khs')
            ->where('nim',$nim)
            ->orderBy('semester','desc')
            ->first();




      Fpdf::AddPage('L');
      Fpdf::SetFont('Arial', 'B', 18);
      Fpdf::Image('https://4.bp.blogspot.com/-nOCPZVY8wdU/XJeti2m55eI/AAAAAAAAAQU/ih5ivB83uLM1IqXEX5OtzjBQdZ-rp_N5QCLcBGAs/s1600/aasasa.png',10,5,-1000);
      Fpdf::Cell(185,7,                 $setting->nama_universitas,0,1,'C');

      Fpdf::SetFont('Arial', '', 12);
      Fpdf::Cell(185,5,$setting->alamat,0,1,'C');
      Fpdf::SetFont('Arial', '', 12);
      Fpdf::Cell(185,5,"Telpon : ".$setting->no_telpon." | Email : ".$setting->email.' | Fax : '.$setting->fax,0,1,'C');

      Fpdf::Line(10,30,190,30);
      Fpdf::Line(10,31,190,31);

      Fpdf::Cell(30,10,'',0,1);
      Fpdf::SetFont('Arial', 'B', 14);
      Fpdf::Cell(185,5,'Jadwal Kuliah',0,0,'C');

      Fpdf::Cell(30,10,'',0,1);
      // biodata mahasiswa
      Fpdf::SetFont('Arial', '', 12);
      Fpdf::Cell(35,5,'NIM',0,0);
      Fpdf::Cell(50,5,' : '.$mahasiswa->nim,0,0);
      Fpdf::Cell(10,5,'',0,0);
      Fpdf::Cell(35,5,'Jurusan',0,0);
      Fpdf::Cell(10,5,' : '.$jurusan->nama_jurusan,0,1);

      Fpdf::Cell(35,5,'Nama',0,0);
      Fpdf::Cell(50,5,' : '.$mahasiswa->nama_mahasiswa,0,0);
      Fpdf::Cell(10,5,'',0,0);
      if(!empty($info)){
      Fpdf::Cell(35,5,'Semester Tempuh',0,0);
      Fpdf::Cell(10,5,' : '.$info->semester,0,1);
    }
    if(empty($info)){
    Fpdf::Cell(35,5,'Semester Tempuh',0,0);
    Fpdf::Cell(10,5,' : '.'1',0,1);
  }

      Fpdf::Cell(35,5,'Tahun Akademik',0,0);
      Fpdf::Cell(50,5,' : '.$tahunaktif->tahun_akademik,0,0);
      Fpdf::Cell(10,5,'',0,0);


      Fpdf::Cell(30,10,'',0,1);


      Fpdf::Cell(10,7,'No',1,0);
      Fpdf::Cell(20,7,'HARI',1,0);
      Fpdf::Cell(25,7,'TEORI',1,0);
      Fpdf::Cell(25,7,'PRAKTIKUM',1,0);
      Fpdf::Cell(80,7,'NAMA MK',1,0);
      Fpdf::Cell(60,7,'DOSEN',1,0);
      Fpdf::Cell(30,7,'KELAS',1,0);
      Fpdf::Cell(30,7,'RUANG',1,1);

      Fpdf::SetFont('Times', '', 11);
      $no         =1;

      foreach($jadwalkuliah as $row)
      {
        $praktikum = DB::table('jadwal_kuliah')
                    ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                    ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                    ->where('jadwal_kuliah.id',$row->id)
                    ->get()->toArray();

          if(empty($praktikum)){


          Fpdf::Cell(10,7,$no++,1,0);
          Fpdf::Cell(20,7,$row->hari,1,0);
          Fpdf::Cell(25,7,$row->jam,1,0);
          Fpdf::Cell(25,7,'-',1,0);
          Fpdf::Cell(80,7,$row->nama_mk,1,0);
          Fpdf::Cell(60,7,$row->nama,1,0);
          Fpdf::Cell(30,7,$row->nama_kelas,1,0);
          Fpdf::Cell(30,7,$row->nama_ruangan,1,1);
        }
        else{
          Fpdf::Cell(10,7,$no++,1,0);
          Fpdf::Cell(20,7,$row->hari,1,0);
          Fpdf::Cell(25,7,$row->jam,1,0);
          Fpdf::Cell(25,7,$praktikum[0]->jam,1,0);
          Fpdf::Cell(80,7,$row->nama_mk,1,0);
          Fpdf::Cell(60,7,$row->nama,1,0);
          Fpdf::Cell(30,7,$row->nama_kelas,1,0);
          Fpdf::Cell(30,7,$row->nama_ruangan,1,1);
        }


      }
      Fpdf::Output('I',"Jadwal Kuliah_$nama");
      exit();

  }
}
