<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;
use DataTables;
use App\Kelompok;
use App\Dosen;

class JurusanController extends Controller
{
    function json(){
        $jurusan = \DB::table('jurusan')
                    ->join('kelompok','jurusan.kode_kelompok','=','kelompok.kode_kelompok')
                    ->join('dosen','jurusan.nidn','=','dosen.nidn')
                    ->get();

                    


        return Datatables::of($jurusan)
        ->addColumn('action', function ($jurusan) {

              return view('modal.jurusan' , [
             'model' => $jurusan,
               'url_edit' => route('jurusan.edit', $jurusan->kode_jurusan),
               'url_destroy' => route('jurusan.destroy', $jurusan->kode_jurusan)
          ]);
      })
      ->rawColumns(['action'])
      ->make(true);

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jurusan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kelompok'] = kelompok::pluck('nama_kelompok','kode_kelompok');
        $data['dosen'] = Dosen::pluck('nama','nidn');
        return view('jurusan.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_jurusan' => 'required|unique:jurusan|min:4',
            'nama_jurusan' => 'required|min:6'
        ]);


        $jurusan = New jurusan();
        $jurusan->create($request->all());
        return redirect('/jurusan')->with('status','Data jurusan Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_jurusan)
    {
        $data['kelompok'] = kelompok::pluck('nama_kelompok','kode_kelompok');
        $data['dosen'] = Dosen::pluck('nama','nidn');
        $data['jurusan'] = jurusan::where('kode_jurusan',$kode_jurusan)->first();
        return view('jurusan.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_jurusan)
    {
        $request->validate([
            'nama_jurusan' => 'required|min:6'
        ]);


        $jurusan = jurusan::where('kode_jurusan','=',$kode_jurusan);
        $jurusan->update($request->except('_method','_token'));
        $jurusan->nidn = $request->nidn;
        return redirect('/jurusan')->with('status','Data jurusan Berhasil Di Update');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_jurusan)
    {
      $kelas = jurusan::where('kode_jurusan',$kode_jurusan);
      $kelas->delete();

    }
}
