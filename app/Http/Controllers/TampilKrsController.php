<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Mahasiswa;
use App\Dosen;
use App\Matakuliah;
use App\Kelas;
use App\Khs;
use App\Krs;
use App\Validasi;
use App\TahunAkademik;
class TampilKrsController extends Controller
{


    function mahasiswa_json(){



      $tahun_akademik =   TahunAkademik::where('tahun_akademik.status','y')->first();
      $data = \DB::table('krs')
      ->where('kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
      ->get();





      foreach ($data as $value) {

          $array[] = $value->nim;
            }


                  if(empty($array)){

              $dataku = \DB::table('pengajuan')
              ->join('mahasiswa','mahasiswa.nim','pengajuan.nim')
              ->join('kelas','kelas.kode_kelas','=','mahasiswa.kode_kelas')
              ->where('pengajuan.tahun_akademik',$tahun_akademik->kode_tahun_akademik)
              ->select('pengajuan.nim','mahasiswa.nama_mahasiswa','kelas.nama_kelas')

              ->get();
            }
            else{
              $dataku = \DB::table('pengajuan')
                      ->join('mahasiswa','mahasiswa.nim','pengajuan.nim')
                      ->join('kelas','kelas.kode_kelas','=','mahasiswa.kode_kelas')
                      ->where('pengajuan.tahun_akademik',$tahun_akademik->kode_tahun_akademik)
                      ->select('pengajuan.nim','mahasiswa.nama_mahasiswa','kelas.nama_kelas')
                      ->whereNotIn('pengajuan.nim',$array)
                      ->get();

            }

              // dd($dataku);



              $filter = $dataku->unique('nim');


        return Datatables::of($filter)
        ->addColumn('action', function ($row) {
            $action  = '<a href="/validasikrs/'.$row->nim.'/show" class="btn btn-primary btn-sm">VALIDASI</a>';
            $action .= \Form::close();
            return $action;
        })
        ->make(true);

    }


    public function index()
    {
        $data['mahasiswa'] = Mahasiswa::pluck('nama_mahasiswa','nim');

        $data['kelas'] = Kelas::pluck('nama_kelas','kode_kelas');



        return view('/validasikrs.index',$data);
    }



    public function show($nim)
    {

      $data['mahasiswa']= \DB::table('mahasiswa')
                          ->join('kelas', 'kelas.kode_kelas','=','mahasiswa.kode_kelas')
                          ->join('dosen','dosen.nidn','=','kelas.nidn')
                          ->where('mahasiswa.nim',$nim)
                          ->first();


        $data['pengajuan'] = \DB::table('pengajuan')
                ->join('mahasiswa','mahasiswa.nim','=','pengajuan.nim')
                ->join('matakuliah','matakuliah.kode_mk','=','pengajuan.kode_mk')
                ->where('pengajuan.nim', $nim)
                ->get();



        // $data['mahasiswa'] = Mahasiswa::pluck('nama_mahasiswa','nim');



        return view('/validasikrs.show', $data);
    }




    public function selesai($nim)
    {

        $pengajuan = \DB::table('pengajuan')
        // ->join('kurikulum','pengajuan.kode_mk','=','kurikulum.kode_mk')
        ->where('pengajuan.nim',$nim)->get();

        $mahasiswa = \DB::table('mahasiswa')
                    ->where('mahasiswa.nim',$nim)
                    ->first();





        $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
        foreach($pengajuan as $row)
        {

            $krs = new Krs;
            $krs->kode_mk   = $row->kode_mk;
            $krs->nim       = $nim;
            $krs->kode_tahun_akademik = $tahun_akademik->kode_tahun_akademik;
            $krs->semester  = $row->semester;
            $krs->kode_jurusan = $mahasiswa->kode_jurusan;
            $krs->save();
          }
            $kode_kelas = \DB::table('mahasiswa')->where('mahasiswa.nim',$nim)->first();

            $validasi = new Validasi;
            $validasi->nim   = $nim;
            $validasi->kode_tahun_akademik = $tahun_akademik->kode_tahun_akademik;
            $validasi->status = "LUNAS";
            $validasi->kode_kelas = $kode_kelas->kode_kelas;

            $validasi->save();



        return redirect('validasikrs')->with('status','Validasi Berhasil');
    }

    function validasi_json(){
        $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
      $jadwal = \DB::table('validasi')
              ->leftJoin('mahasiswa','mahasiswa.nim','=','validasi.nim')
              ->leftJoin('kelas','kelas.kode_kelas','=','validasi.kode_kelas')
              ->where('validasi.kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
              ->get();
              return Datatables::of($jadwal)
          ->make(true);

    }

    public function validasi_index(){
      $data['mahasiswa'] = Mahasiswa::pluck('nama_mahasiswa','nim');

      $data['kelas'] = Kelas::pluck('nama_kelas','kode_kelas');

      return view('/validasikrs.selesai',$data);
    }

}
