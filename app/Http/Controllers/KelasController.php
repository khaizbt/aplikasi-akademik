<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\Kelas;
use DataTables;
use App\Mahasiswa;
use App\Jurusan;
class KelasController extends Controller
{
    function json(){
      $model = \DB::table('kelas')
                    ->join('dosen','dosen.nidn','=','kelas.nidn')
                    ->get();
                      return DataTables::of($model)
                      ->addColumn('action', function ($model) {
                        '<a href="/matakuliah/'.$model->kode_kelas.'/edit" class="btn btn-primary btn-sm">EDIT</a>';
                            return view('modal.kelas' , [
                           'model' => $model,
                             'url_edit' => route('kelas.edit', $model->kode_kelas),
                             'url_destroy' => route('kelas.destroy', $model->kode_kelas)
                        ]);
                    })
                ->rawColumns(['action'])
                ->make(true);
              }
        




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kelas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['dosen'] = Dosen::pluck('nama','nidn');
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        return view('kelas.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_kelas' => 'required|unique:kelas|min:4',
            'nama_kelas' => 'required|min:4',
            'nidn' => 'required'
        ]);


        $kelas = New kelas();
        $kelas->create($request->all());
        return redirect('/kelas')->with('status','Data kelas Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_kelas)
    {
        $data['dosen'] = Dosen::pluck('nama','nidn');
        $data['kelas'] = Kelas::where('kode_kelas',$kode_kelas)->first();
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        return view('kelas.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_kelas)
    {
        $request->validate([
            'nama_kelas' => 'required|min:3',
            'nidn' => 'required|min:3'
        ]);


        $kelas = kelas::where('kode_kelas','=',$kode_kelas);
        $kelas->update($request->except('_method','_token'));
        return redirect('/kelas')->with('status','Data kelas Berhasil Di Update');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_kelas)
    {
        $kelas = Kelas::where('kode_kelas',$kode_kelas);
        $kelas->delete();


        // return redirect('/kelas')->with('status','Data kelas Berhasil Dihapus');


    }

    function hapuskelas(Request $request){

        $krs = Kelas::find($request->kode_kelas);
        $krs->delete();
    }



}
