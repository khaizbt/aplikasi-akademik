<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Excel;
use App\Khs;
use App\Imports\NilaiImport;
use App\Exports\NilaiExport;

class NilaiController extends Controller
{
    function index($id_jadwal){



        $tahunaktif = \DB::table('tahun_akademik')
                      ->where('tahun_akademik.status','y')->first();

         $dosen = Auth::guard('dosen')->user()->nidn;

        $jadwal = \DB::table('jadwal_kuliah')
                ->leftJoin('dosen','dosen.nidn','=','jadwal_kuliah.nidn')
                ->leftJoin('matakuliah','matakuliah.kode_mk','=','jadwal_kuliah.kode_mk')
                ->where('jadwal_kuliah.nidn',$dosen)
                ->where('kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
                ->where('id',$id_jadwal)
                ->first();


                $data['kontrak'] = \DB::table('kontrak')
                       ->join('dosen','dosen.nidn','=','kontrak.nidn')
                       ->join('matakuliah','matakuliah.kode_mk','=','kontrak.kode_mk')
                                           ->where('kontrak.nidn', $dosen)
                                           ->where('kontrak.id_jadwal',$id_jadwal)
                                           ->get();



        $data['mahasiswa'] = \DB::table('khs')
                            ->join('mahasiswa','mahasiswa.nim','=','khs.nim')
                            ->where('khs.nidn',$dosen)
                            ->where('khs.kode_mk',$jadwal->kode_mk)
                            ->where('khs.kode_kelas',$jadwal->kode_kelas)
                            ->get();
        $nilai = \DB::table('khs')
                      ->where('nidn',$dosen)
                      ->where('id_jadwal',$id_jadwal)
                      ->get();
                      $filter['khs'] = $nilai->unique('id_jadwal');






        $data['jadwal']    = $jadwal;


        return view('nilai.index',$data,$filter);
    }

    function update_nilai(request $request)
    {
        $nilai = \DB::table('khs')
                ->where('id',$request->id_khs)
                ->update([
                    'nilai_uas'         =>  $request->nilai_uas,
                    'nilai_uts'         =>  $request->nilai_uts,
                    'nilai_kehadiran'   =>  $request->nilai_kehadiran,
                    'nilai_tugas'       =>  $request->nilai_tugas
                    ]);
    }

    function update_kontrak(request $request)
    {
        $kontrak = \DB::table('kontrak')
                ->where('id',$request->id_kontrak)
                ->update([
                    'kontrak_uas'         =>  $request->kontrak_uas,
                    'kontrak_uts'         =>  $request->kontrak_uts,
                    'kontrak_kehadiran'   =>  $request->kontrak_kehadiran,
                    'kontrak_tugas'       =>  $request->kontrak_tugas,
                    ]);
    }

    public function ImportIndex($id){

      $data['khs'] = \DB::table('khs')
                    ->where('id_jadwal',$id)
                    ->first();

      return view('nilai.import',$data);
    }

    public function Import($id){


      $data = Excel::toArray(new NilaiImport, request()->file('file'));
      $dosen = Auth::guard('dosen')->user()->nidn;

    collect(head($data))
        ->each(function ($row, $key) {

            \DB::table('khs')
                ->where('nim', $row['nim'])
                ->where('kode_mk', $row['kode_mk'])
                ->where('nidn', $row['nidn'])
                ->update(array_except($row, ['nim','semester','nidn','kode_mk','nama_mahasiswa']));
        });
        return redirect('/nilai/'.$id)->with('status','Data Nilai Berhasil Di Update');

    }

    function download($id){

      $headings = [
            'nim',
            'nama mahasiswa',
            'nilai_kehadiran',
            'nilai_tugas',
            'nilai_uts',
            'nilai_uas',
            'nidn',
            'kode_mk',
            'kode_tahun_akademik'

        ];

        // return (new NilaiExport($id,$headings))->download('_inventory.xlsx');
    return Excel::download(new NilaiExport($id,$headings), 'Nilai.xlsx');
    }

    function penilaian(){
      $tahunaktif = \DB::table('tahun_akademik')
                    ->where('tahun_akademik.status','y')->first();
      $date1 = strtotime("today");
      $date2 = strtotime($tahunaktif->tanggal_akhir_kuliah);

      if ($date1 > $date2){
      $id = \DB::table('khs')
                ->where('kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
                ->groupBy('id_jadwal')
                ->select('id_jadwal')
                ->get();


      foreach($id as $row) {
        $hitungkehadiran = \DB::table('khs')
                  ->where('id_jadwal',$row->id_jadwal)
                  ->where('nilai_kehadiran',0)
                  ->select('id_jadwal')
                  ->count();

                  $hitungsemua = \DB::table('khs')
                                      ->where('id_jadwal',$row->id_jadwal)
                                      ->select('id_jadwal')
                                      ->count();


                    if ($hitungkehadiran == $hitungsemua) {


                              $nilai = \DB::table('khs')
                                      ->where('id_jadwal',$row->id_jadwal)
                                      ->update([
                                          'nilai_uas'         =>  80,
                                          'nilai_uts'         =>  80,
                                          'nilai_kehadiran'   =>  80,
                                          'nilai_tugas'       =>  80
                                          ]);

                    }

                  }
                }


    }

}
