<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function index_mahasiswa()
    {
      $tahun_akademik = \DB::table('tahun_akademik')
                        ->where('status','y')
                        ->first();
      $awal_krs = DateTime::createFromFormat('Y-m-d', $tahun_akademik->tanggal_awal_krs);
      $akhir_krs = DateTime::createFromFormat('Y-m-d', $tahun_akademik->tanggal_akhir_krs);
      $awal_pengajuan = DateTime::createFromFormat('Y-m-d', $tahun_akademik->tanggal_awal_pengajuan);
      $akhir_pengajuan = DateTime::createFromFormat('Y-m-d', $tahun_akademik->tanggal_akhir_pengajuan);


        return view('mahasiswa.home', ['tahun' => $tahun_akademik],['tanggal_awal_krs'=> $awal_krs->format('d-M-Y'),'tanggal_akhir_krs'=> $akhir_krs->format('d-M-Y'),'tanggal_awal_pengajuan'=> $awal_pengajuan->format('d-M-Y'),'tanggal_akhir_pengajuan'=> $akhir_pengajuan->format('d-M-Y')]);
    }
    public function index_dosen()
    {
      return view('dosen.home');
    }
}
