<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DataTables;
use App\Jurusan;
use App\Mahasiswa;
use App\TahunAkademik;
use App\Kelas;
use Fpdf;
use Storage;
use Excel;
use DateTime;
use App\Exports\MahasiswaExport;
use App\Imports\MahasiswaImport;



class MahasiswaController extends Controller
{
    function json(){

            $mahasiswa = \DB::table('mahasiswa')
            ->join('jurusan','mahasiswa.kode_jurusan','=','jurusan.kode_jurusan')
            ->join('kelompok','kelompok.kode_kelompok','=','jurusan.kode_kelompok')
            ->get();



        return Datatables::of($mahasiswa)
          ->addColumn('action', function ($mahasiswa) {
        '<a href="/mahasiswa/'.$mahasiswa->nim.'/edit" class="btn btn-primary btn-sm">EDIT</a>';
            return view('modal.mahasiswa' , [
           'model' => $mahasiswa,
           'url_show' => route('mahasiswa.show',$mahasiswa->nim),
             'url_edit' => route('mahasiswa.edit', $mahasiswa->nim),
             'url_destroy' => route('mahasiswa.destroy', $mahasiswa->nim)
        ]);
    })
    ->rawColumns(['action'])
    ->make(true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tahun_akademik'] = TahunAkademik::pluck('tahun_akademik','kode_tahun_akademik');
        $data['jurusan']        = Jurusan::pluck('nama_jurusan','kode_jurusan');
        return view('mahasiswa.index_ajax',$data);
        //return view('mahasiswa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['tahun_akademik'] = TahunAkademik::pluck('tahun_akademik','kode_tahun_akademik');
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['tahun_akademik'] = \DB::table('tahun_akademik')->where('status','y')->first();
        $data['kelas'] = Kelas::pluck('nama_kelas','kode_kelas');
        return view('mahasiswa.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'nama_mahasiswa' => 'required|min:6',
            'email' => 'required|min:6',
            // 'password' => 'required|min:6',
            'tempat_lahir' => ' required|min:6',
            'tanggal_lahir' => ' required|min:6',
            'alamat' => ' required|min:6',
            'no_hp' => ' required|min:8',

        ]);

        $hitungkelas = \DB::table('mahasiswa')
                        ->join('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                        ->where('mahasiswa.kode_kelas','<=',50)
                        ->where('kelas.jurusan',$request->kode_jurusan)
                        ->first();



        $tahun = \DB::table('tahun_akademik')
                ->where('status','y')
                ->first();

        $jurusan = \DB::table('jurusan')
                  ->where('kode_jurusan',$request->kode_jurusan)
                  ->first();



        $sub_tahun = substr($tahun->created_at, 2,2);



         $users = \DB::table('mahasiswa')
                       ->where('kode_jurusan',$request->kode_jurusan)
                       ->max('nim');


          $sub_cek  = substr($users,0,2);

          if($sub_cek == $sub_tahun){
            $sub_kalimat = substr($users,7);
            $sub_kalimat++;
            $test = sprintf("%04s", $sub_kalimat);


          }

            else{
              $sub_kalimat = "0000";
              $sub_kalimat++;
              $test = sprintf("%04s", $sub_kalimat);




            }






        $mahasiswa = New mahasiswa();
        $mahasiswa->nim = $sub_tahun.'.'.$jurusan->no_jurusan.'.'.$test;
        $mahasiswa->nama_mahasiswa = $request->nama_mahasiswa;
        $mahasiswa->email = $request->email;
        $mahasiswa->no_hp = $request->no_hp;
        $mahasiswa->alamat = $request->alamat;
        $mahasiswa->jenis_kelamin = $request->jenis_kelamin;
        $mahasiswa->tanggal_lahir = $request->tanggal_lahir;
        $mahasiswa->tempat_lahir = $request->tempat_lahir;
        $mahasiswa->kode_jurusan = $request->kode_jurusan;
        $mahasiswa->nama_ibu = $request->nama_ibu;
        $mahasiswa->nama_ayah = $request->nama_ayah;
        $mahasiswa->kode_tahun_akademik = $tahun->kode_tahun_akademik;
        $mahasiswa->kode_kelas = $hitungkelas->kode_kelas; //Ganti Besok
        $mahasiswa->password = '123456';
        $mahasiswa->save();
        return redirect('/mahasiswa')->with('status','Data mahasiswa Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($nim)
     {
         $user = \App\Mahasiswa::findOrFail($nim);



         $mahasiswa = \DB::table('mahasiswa')
                      ->join('jurusan','mahasiswa.kode_jurusan','=','jurusan.kode_jurusan')
                      ->join('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                      ->join('dosen','kelas.nidn','=','dosen.nidn')
                      ->where('mahasiswa.nim',$nim)
                      ->first();

         if(empty($user->tanggal_lahir)){
           $tanggal = DateTime::createFromFormat('Y-m-d', "1997-10-09");
          return view('mahasiswa.show',['mahasiswa' => $user,'jurusan' => $mahasiswa],['tanggal_lahir'=> $tanggal->format('d-M-Y')]);
         }
         else{
           $tanggal = DateTime::createFromFormat('Y-m-d', $user->tanggal_lahir);
          return view('mahasiswa.show',['mahasiswa' => $user,'jurusan' => $mahasiswa],['tanggal_lahir'=> $tanggal->format('d-M-Y')]);
         }

     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nim)
    {
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['mahasiswa'] = Mahasiswa::where('nim','=',$nim)->first();
        $data['mahasiswa_terpilih'] = \DB::table('mahasiswa')
                            ->join('jurusan','mahasiswa.kode_jurusan','=','jurusan.kode_jurusan')
                            ->join('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                            ->where('nim',$nim)
                            ->get();
        $data['tahun_akademik'] = TahunAkademik::pluck('tahun_akademik','kode_tahun_akademik');
        $data['jurusan']        = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['semester_aktif'] = ['1'=>'Semester 1','2'=>'Semester 2','3'=>'Semester 3',
        '4'=>'Semester 4','5'=>'semester 5','6'=>'Semester 6','7'=>'Semester 7','8'=>'Semester 8'];
        $data['kelas'] = Kelas::pluck('nama_kelas','kode_kelas');

        return view('mahasiswa.edit',$data);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nim)
    {
      $request->validate([

          'nama_mahasiswa' => 'required|min:6',
          'email' => 'required|min:6',
          // 'password' => 'required|min:6',
          'tempat_lahir' => ' required|min:6',
          'tanggal_lahir' => ' required|min:6',
          'alamat' => ' required|min:6',
          'no_hp' => ' required|min:8',

      ]);





        $mahasiswa = Mahasiswa::where('nim','=',$nim)->first();
        $mahasiswa->nama_mahasiswa = $request->nama_mahasiswa;
        $mahasiswa->email = $request->email;
        $mahasiswa->no_hp = $request->no_hp;
        $mahasiswa->alamat = $request->alamat;
        $mahasiswa->jenis_kelamin = $request->jenis_kelamin;
        $mahasiswa->tanggal_lahir = $request->tanggal_lahir;
        $mahasiswa->tempat_lahir = $request->tempat_lahir;

        if($request->password!='')
        {
            $mahasiswa->password = $request->password;
        }

        $mahasiswa->save();
        //$mahasiswa->update($request->except('_method','_token'));
        return redirect('/mahasiswa')->with('status','Data mahasiswa Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nim)
    {
        $mahasiswa = mahasiswa::where('nim',$nim);
        $mahasiswa->delete();

    }

    public function pdf(Request $request)
    {
      $jurusan  = $request->get('jurusan');
      $tahun_akademik = $request->get('tahun_akademik');


      $data['mahasiswa'] = \DB::table('mahasiswa')
                  ->leftJoin('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                  ->leftJoin('kelas','kelas.kode_kelas','=','mahasiswa.kode_kelas')
                  ->leftJoin('tahun_akademik','mahasiswa.kode_tahun_akademik','=','tahun_akademik.kode_tahun_akademik')
                  ->leftJoin('dosen','kelas.nidn','=','dosen.nidn')
                  ->where('mahasiswa.kode_jurusan',$jurusan)
                  ->where('mahasiswa.kode_tahun_akademik',$tahun_akademik)
                  ->get();



      $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
      $data['tahun_akademik'] = TahunAkademik::pluck('tahun_akademik','kode_tahun_akademik');
      $data['jurusan_terpilih'] = $jurusan;
      $data['tahun_terpilih'] = $tahun_akademik;

      return view('mahasiswa.pdf',$data);
    }

    function downPdf(){
      $mahasiswa = \DB::table('mahasiswa')
                  ->leftJoin('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                  ->leftJoin('kelas','kelas.kode_kelas','=','mahasiswa.kode_kelas')
                  ->leftJoin('tahun_akademik','mahasiswa.kode_tahun_akademik','=','tahun_akademik.kode_tahun_akademik')
                  ->leftJoin('dosen','kelas.nidn','=','dosen.nidn')
                  ->get();

                  Fpdf::AddPage();

                  Fpdf::Cell(30,10,'',0,1);
                  Fpdf::SetFont('Arial', 'B', 14);
                  Fpdf::Cell(185,5,'Daftar Mahasiswa',0,0,'C');
                  Fpdf::Cell(30,10,'',0,1);
                  Fpdf::SetFont('Arial','',12);
                  $no=1;
                  foreach($mahasiswa as $value){

                    Fpdf::Cell(10,7,$no,1,0);
                    Fpdf::Cell(25,7,$value->nim,1,0);
                    Fpdf::Cell(50,7,$value->nama_mahasiswa,1,0);
                    Fpdf::Cell(60,7,$value->nama_jurusan,1,0);
                    Fpdf::Cell(50,7,$value->nama_kelas,1,1);
                    $no++;
                }

                  //save file
                  Fpdf::Output('I',"Daftar Semua Mahasiswa");
                  exit();
    }

    public function mhspdf(Request $request)
    {
      $jurusan  = $request->get('jurusan');
      $tahun_akademik = $request->get('tahun_akademik');


      $mahasiswa = \DB::table('mahasiswa')
                  ->leftJoin('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                  ->leftJoin('kelas','kelas.kode_kelas','=','mahasiswa.kode_kelas')
                  ->leftJoin('tahun_akademik','mahasiswa.kode_tahun_akademik','=','tahun_akademik.kode_tahun_akademik')
                  ->leftJoin('dosen','kelas.nidn','=','dosen.nidn')
                  ->where('mahasiswa.kode_jurusan',$jurusan)
                  ->where('mahasiswa.kode_tahun_akademik',$tahun_akademik)
                  ->get();



      $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
      $data['tahun_akademik'] = TahunAkademik::pluck('tahun_akademik','kode_tahun_akademik');
      $data['jurusan_terpilih'] = $jurusan;
      $data['tahun_terpilih'] = $tahun_akademik;




  Fpdf::AddPage();

  Fpdf::Cell(30,10,'',0,1);
  Fpdf::SetFont('Arial', 'B', 14);
  Fpdf::Cell(185,5,'Daftar Mahasiswa',0,0,'C');
  Fpdf::Cell(30,10,'',0,1);
  Fpdf::SetFont('Arial','',12);
  $no=1;
  foreach($mahasiswa as $key){

    Fpdf::Cell(10,7,$no,1,0);
    Fpdf::Cell(25,7,$key->nim,1,0);
    Fpdf::Cell(50,7,$key->nama_mahasiswa,1,0);
    Fpdf::Cell(60,7,$key->nama_jurusan,1,0);
    Fpdf::Cell(60,7,$key->nama_kelas,1,1);
    $no++;
}

  //save file
  Fpdf::Output('I',"DaftarMahasiswa $jurusan $tahun_akademik");
  exit();

    }


     function importIndex()
      {
          return view('mahasiswa.import');
      }

      function download(){

        $headings = [
              'nim',
              'nama mahasiswa',
              'kode_kelas',
              'alamat',
              'kode_jurusan',
              'no_hp',
              'tempat_lahir',
              'tanggal_lahir'

          ];

          // return (new NilaiExport($id,$headings))->download('_inventory.xlsx');
      return Excel::download(new MahasiswaExport($headings), 'UploadMahasiswa.xlsx');
      }



      public function storeData(Request $request)
        {
            //VALIDASI
            $this->validate($request, [
                'file' => 'required|mimes:xls,xlsx'
            ]);

            if ($request->hasFile('file')) {
                $file = $request->file('file');

                // dd($file); //GET FILE;
                Excel::import(new MahasiswaImport, $file); //IMPORT FILE
                return redirect('/mahasiswa')->with(['status' => 'Upload success']);
            }
            return redirect('/mahasiswa')->with(['error' => 'Please choose file before']);
        }


}
