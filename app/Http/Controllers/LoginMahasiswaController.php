<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginMahasiswaController extends Controller
{
    // menampilkan form login
    function index(){
      if (Auth::guard('mahasiswa')->check()) {
              return redirect ('/mahasiswa/home');
            }

            else{
        return view('mahasiswa.login');
      }
    }



    // chek proses login
    function submit(Request $request)
    {
      //   // Validate the form data
      //    $this->validate($request, [

      // ]);
      

      // Attempt to log the user in
      if (Auth::guard('mahasiswa')->attempt(['nim' => $request->nim, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended('/jadwal_kuliah');
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect('/')->with('status','NIM atau Password Salah,Login DITOLAK ')->withInput($request->only('nim', 'remember'));
    }
}
