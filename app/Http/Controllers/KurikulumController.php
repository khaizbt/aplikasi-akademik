<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;
use App\Matakuliah;
use App\Kurikulum;

class KurikulumController extends Controller
{
    function index(Request $request){
        $jurusan = $request->get('jurusan');

        $data['kurikulum'] =  \DB::table('kurikulum')
                             ->join('matakuliah','matakuliah.kode_mk','=','kurikulum.kode_mk')
                             ->join('jurusan','jurusan.kode_jurusan','=','kurikulum.kode_jurusan')
                             ->where('kurikulum.kode_jurusan',$jurusan)
                             ->get();


        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['jurusan_terpilih'] = $jurusan;
        return view('kurikulum.index',$data);
    }


    function create(){
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['matakuliah'] = Matakuliah::pluck('nama_mk','kode_mk');
        return view('kurikulum.create',$data);
    }

    function store(Request $request){

        $kurikulum = new Kurikulum();
        $kurikulum->kode_mk = $request->kode_mk;
        $kurikulum->kode_jurusan = $request->kode_jurusan;
        $kurikulum->semester = $request->semester;
        $kurikulum->save();


        return redirect('/kurikulum?jurusan='.$kurikulum->kode_jurusan)->with('status','Kurikulum Berhasil Disimpan');
    }

    function edit($id){
      $data['matkul'] = \DB::table('kurikulum')
                          ->join('matakuliah','kurikulum.kode_mk','=','matakuliah.kode_mk')
                          ->where('id',$id)
                          ->get();
      $data['kurikulum'] = Kurikulum::where('id',$id)->first();
      $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
      $data['matakuliah'] = Matakuliah::pluck('nama_mk','kode_mk');
      return view('kurikulum.edit',$data);
    }

    function update(Request $request, $id){

        $kurikulum = Kurikulum::where('id','=',$id);
      $kurikulum->update($request->except('_method','_token'));

        $jadwal = \DB::table('kurikulum')
                  ->where('id',$id)->first();

        
        return redirect('/kurikulum?jurusan='.$jadwal->kode_jurusan)->with('status','Kurikulum Berhasil Disimpan');
    }

    function show($id){
        $kurikulum = Kurikulum::find($id);
        $kurikulum->delete();
        return redirect()->back()->with('status','Data Kurikuum Berhasil Dihapus');
    }
}
