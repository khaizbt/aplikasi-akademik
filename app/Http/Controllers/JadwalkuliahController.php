<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;
use App\Matakuliah;
use App\Dosen;
use App\Ruangan;
use App\Jamkuliah;
use App\Jadwalkuliah;
use App\Kelas;
use App\TahunAkademik;
use App\Kurikulum;
use App\Kontrak;
class JadwalkuliahController extends Controller
{
    function index(Request $request){

        $jurusan  = $request->get('jurusan');
        $semester = $request->get('semester');


        $data['jadwal'] = \DB::table('jadwal_kuliah')
                    ->leftJoin('matakuliah','matakuliah.kode_mk','=','jadwal_kuliah.kode_mk')
                    ->leftJoin('dosen','dosen.nidn','=','jadwal_kuliah.nidn')
                    ->leftJoin('ruangan','ruangan.kode_ruangan','=','jadwal_kuliah.kode_ruangan')
                    ->leftJoin('jam_kuliah','jam_kuliah.id','=','jadwal_kuliah.jam')
                    ->leftJoin('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                    ->select('jadwal_kuliah.id','jadwal_kuliah.kode_mk','jadwal_kuliah.hari','jam_kuliah.jam','matakuliah.nama_mk','dosen.nama','ruangan.nama_ruangan','kelas.nama_kelas')


                    ->where('jadwal_kuliah.kode_jurusan',$jurusan)
                    ->where('jadwal_kuliah.semester',$semester)
                    ->get();



        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['jurusan_terpilih'] = $jurusan;
        $data['semester_terpilih'] = $semester;


        return view('jadwalkuliah.index',$data);
    }
    function show($id){
      $jadwal=Jadwalkuliah::find($id);
        $jadwal->delete();

        return redirect()->back()->with('status','Data Jadwal Berhasil Dihapus');
    }


    function create(){
        $data['matakuliah'] = Matakuliah::pluck('nama_mk','kode_mk');
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['dosen'] = Dosen::pluck('nama','nidn');
        $data['ruangan'] = Ruangan::pluck('nama_ruangan','kode_ruangan');
        $data['jamkuliah'] = Jamkuliah::pluck('jam','id');
        $data['hari'] = ['senin'=>'senin','selasa'=>'selasa','rabu'=>'rabu','kamis'=>'kamis','jumat'=>'jumat','sabtu'=>'sabtu','minggu'=>'minggu'];
        $data['kelas'] = Kelas::pluck('nama_kelas','kode_kelas');
        $data['tahun_akademik'] = \DB::table('tahun_akademik')->where('status','y')->first();
        $data['ruang_praktik'] = Ruangan::pluck('nama_ruangan','kode_ruangan');
        $data['jam_praktik'] = Jamkuliah::pluck('jam','id');
        return view('jadwalkuliah.create',$data);
    }

    function store(request $request)
    {
      $request->validate([
          'kode_mk' => 'required',
          'nidn' => 'required',
          // 'tempat_lahir' => 'required|min:6',
          // 'tanggal_lahir' => 'required|min:6',
          // 'email'     =>'required|unique:dosen',
          // 'alamat'     =>'required',
          // 'no_hp'     =>'required'
      ]);

      $sama = \DB::table('jadwal_kuliah')
              ->where('nidn',$request->nidn)
              ->where('kode_mk',$request->kode_mk)
              ->where('kode_jurusan',$request->kode_jurusan)
              ->where('kode_kelas',$request->kode_kelas)
              ->first();



              if(!empty($sama)){
                return redirect()->back()->with('danger','Data Jadwal Duplikat');
              }

              else
              {

                if($request->remember=="on"){
                  $jadwal = new Jadwalkuliah();
                  $jadwal->kode_mk = $request->kode_mk;
                  $jadwal->nidn = $request->nidn;
                  $jadwal->semester = $request->semester;
                  $jadwal->jam = $request->jam;
                  $jadwal->hari = $request->hari;
                  $jadwal->kode_jurusan = $request->kode_jurusan;
                  $jadwal->kode_ruangan = $request->kode_ruangan;
                  $jadwal->kode_tahun_akademik = $request->kode_tahun_akademik;
                  $jadwal->kode_kelas = $request->kode_kelas;
                  $jadwal->ruang_praktik = $request->ruang_praktik;
                  $jadwal->jam_praktik = $request->jam_praktik;

                  $jadwal->save();
                }
                else{

                  $jadwal = new Jadwalkuliah();
                  $jadwal->kode_mk = $request->kode_mk;
                  $jadwal->nidn = $request->nidn;
                  $jadwal->semester = $request->semester;
                  $jadwal->jam = $request->jam;
                  $jadwal->hari = $request->hari;
                  $jadwal->kode_jurusan = $request->kode_jurusan;
                  $jadwal->kode_ruangan = $request->kode_ruangan;
                  $jadwal->kode_tahun_akademik = $request->kode_tahun_akademik;
                  $jadwal->kode_kelas = $request->kode_kelas;
                  $jadwal->save();
                }

                    $kontrak = new Kontrak();
                    $kontrak->nidn        = $request->nidn;
                    $kontrak->kode_mk     = $request->kode_mk;
                    $kontrak->semester     = $request->semester;
                    $kontrak->id_jadwal = $jadwal->id;
                    $kontrak->kode_kelas   = $request->kode_kelas;
                    // $kontrak->id_jadwal = $jadwal->id;
                    $kontrak->save();
                    return redirect('/jadwalkuliah?jurusan='.$jadwal->kode_jurusan.'&semester='.$jadwal->semester)->with('status','Jadwal Kuliah Berhasil Disimpan');
                }
              


    }

    public function edit($id)
    {
        $data['matakuliah'] = Matakuliah::pluck('nama_mk','kode_mk');
        $data['dosen'] = Dosen::pluck('nama','nidn');
        $data['jurusan'] = Jurusan::pluck('nama_jurusan','kode_jurusan');
        $data['tahun_akademik'] = TahunAkademik::pluck('tahun_akademik','kode_tahun_akademik');
        $data['ruangan'] = Ruangan::pluck('nama_ruangan','kode_ruangan');
        $data['hari'] = ['senin'=>'senin','selasa'=>'selasa','rabu'=>'rabu','kamis'=>'kamis','jumat'=>'jumat','sabtu'=>'sabtu','minggu'=>'minggu'];
        $data['jamkuliah'] = Jamkuliah::pluck('jam','id');
        $data['kelas'] = Kelas::pluck('nama_kelas','kode_kelas');
        $data['tahun_akademik'] = \DB::table('tahun_akademik')->where('status','y')->first();
        $data['jadwal_kuliah'] = Jadwalkuliah::where('id',$id)->first();
        $data['ruang_praktik'] = Ruangan::pluck('nama_ruangan','kode_ruangan');
        $data['jam_praktik'] = Jamkuliah::pluck('jam','id');

        $data['matakuliah_terpilih'] = \DB::table('jadwal_kuliah')
                                      ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                                      ->join('jurusan','jadwal_kuliah.kode_jurusan','=','jurusan.kode_jurusan')
                                      ->where('id',$id)->get();




        return view('jadwalkuliah.edit',$data);
    }


    public function update(Request $request, $id)
    {

        $jadwal_kuliah = Jadwalkuliah::where('id','=',$id);
        $jadwal_kuliah->update($request->except('remember','_method','_token'));


        $kontrak = Kontrak::where('id_jadwal','=',$id)->first();
        $kontrak->nidn = $request->nidn;
        $kontrak->semester     = $request->semester;
        // $kontrak->id_jadwal = $jadwal_kuliah->id;
        $kontrak->kode_kelas   = $request->kode_kelas;
        $kontrak->save();

        $jadwal = Jadwalkuliah::findOrFail($id)->first();




        return redirect('/jadwalkuliah?jurusan='.$jadwal->kode_jurusan.'&semester='.$jadwal->semester)->with('status','Jadwal Kuliah Berhasil Diubah');
    }




    public function destroy($id)
    {
      $jadwal=Jadwalkuliah::find($id);
        $jadwal->delete();

       return redirect()->back()->with('status','Data jurusan Berhasil Dihapus');;
        // $jadwal = Jadwalkuliah::where('id',$id);
        // $jadwal->delete();
        // return redirect('/jurusan')->with('status','Data jurusan Berhasil Dihapus');;
    }


}
