<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Matakuliah;
use App\Krs;
use App\Pengajuan;
use Auth;
use DB;
use Fpdf;
use App\Khs;
use App\Jadwalkuliah;


class KrsController extends Controller
{

    function __construct()
    {
        return $this->middleware('auth:mahasiswa');
    }

    function DaftarMatakuliahKontrak(){
      $result = '<table class="table table-bordered">
              <tr><th width="230">Nama Matakuliah</th><th width="250">Nama Dosen</th><th width="80">hari</th><th width="130">Teori(Jam/Ruang)</th><th width="130">Praktikum(Jam/Ruang)</th><th width="130">Kelas</th><th>Action</th<tr>';

      $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
      $nim = Auth::guard('mahasiswa')->user()->nim;
      $jurusan = Auth::guard('mahasiswa')->user()->kode_jurusan;


      $krs = DB::table('khs')
             ->where('nim',$nim)
             ->get();

             foreach($krs as $values){
               $matkul[] = $values->kode_mk;
             }



      $kelas = \DB::table('khs')
                ->where('kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
                ->groupBy('id_jadwal')
                ->select('id_jadwal')
                ->get();

              foreach($kelas as $row) {
                $hitung = \DB::table('khs')
                          ->where('id_jadwal',$row->id_jadwal)
                          ->select('id_jadwal')
                          ->count();

                            if ($hitung >= 50) {

                              $dataku = DB::table('jadwal_kuliah')
                                      ->join('krs','jadwal_kuliah.kode_mk','=','krs.kode_mk')
                                      ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                                      ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                                      ->where('jadwal_kuliah.id',$row->id_jadwal)
                                      ->where('jadwal_kuliah.kode_jurusan',$jurusan)
                                      ->where('jadwal_kuliah.kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
                                      ->select('jadwal_kuliah.id','krs.kode_mk','krs.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','jadwal_kuliah.kode_tahun_akademik')
                                      ->get();
                                      $arr[] = $dataku[0];

                                     //Daftar Matakuliah yang sudah limit

                            }


                          }




                          if(!empty($arr)){
                          foreach ($arr as $val) {


                              $dataw[] = $val->id;
                                }
                              }


                                if(empty($matkul) and empty($arr) )
                                {
                                $semua =  DB::table("jadwal_kuliah")

                                    ->join('krs','jadwal_kuliah.kode_mk','=','krs.kode_mk')
                                    ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                                    ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                                    ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                                    ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                                    ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                                    ->where('krs.nim',$nim)
                                    ->where('jadwal_kuliah.kode_jurusan',$jurusan)
                                    ->select('jadwal_kuliah.id','krs.kode_mk','krs.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','krs.nim','ruangan.nama_ruangan','jam_kuliah.jam','kelas.nama_kelas')
                                    ->get();
                                  }
                                  elseif(!empty($matkul) and !empty($arr)){
                                    $semua =  DB::table("jadwal_kuliah")

                                        ->join('krs','jadwal_kuliah.kode_mk','=','krs.kode_mk')
                                        ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                                        ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                                        ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                                        ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                                        ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                                        ->where('krs.nim',$nim)
                                        ->where('jadwal_kuliah.kode_jurusan',$jurusan)
                                        ->whereNotIn('jadwal_kuliah.id',$dataw)
                                        ->whereNotIn('jadwal_kuliah.kode_mk',$matkul)
                                        ->select('jadwal_kuliah.id','krs.kode_mk','krs.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','krs.nim','ruangan.nama_ruangan','jam_kuliah.jam','kelas.nama_kelas')
                                        ->get();

                                        foreach($semua as $kabeh){
                                          $itung = \DB::table('khs')
                                                    ->where('id_jadwal',$kabeh->id)
                                                    ->select('id_jadwal')
                                                    ->count();
                                        }
                                      }
                                  elseif(!empty($dataw))
                                  {
                                    $semua =  DB::table("jadwal_kuliah")

                                        ->join('krs','jadwal_kuliah.kode_mk','=','krs.kode_mk')
                                        ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                                        ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                                        ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                                        ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                                        ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                                        ->where('krs.nim',$nim)
                                        ->where('jadwal_kuliah.kode_jurusan',$jurusan)
                                        ->whereNotIn('jadwal_kuliah.id',$dataw)
                                        // ->whereNotIn('jadwal_kuliah.kode_mk',$matkul)
                                        ->select('jadwal_kuliah.id','krs.kode_mk','krs.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','krs.nim','jadwal_kuliah.kode_ruangan','ruangan.nama_ruangan','jam_kuliah.jam','kelas.nama_kelas')
                                        ->get();

                                        $semuanya = DB::table('jadwal_kuliah')
                                                    ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                                                    ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                                                    ->select('ruangan.nama_ruangan','jam_kuliah.jam')
                                                    ->get();


                                  }
                                  elseif(!empty($matkul) )
                                  {
                                    $semua =  DB::table("jadwal_kuliah")

                                        ->join('krs','jadwal_kuliah.kode_mk','=','krs.kode_mk')
                                        ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                                        ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                                        ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                                        ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                                        ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                                        ->where('krs.nim',$nim)
                                        ->where('jadwal_kuliah.kode_jurusan',$jurusan)
                                        ->whereNotIn('jadwal_kuliah.kode_mk',$matkul)
                                        // ->whereNotIn('jadwal_kuliah.id',$dataw)
                                        ->select('jadwal_kuliah.id','krs.kode_mk','krs.semester','dosen.nama','jadwal_kuliah.hari','jadwal_kuliah.jam','jadwal_kuliah.nidn','jadwal_kuliah.kode_kelas','matakuliah.nama_mk','krs.nim','jadwal_kuliah.kode_ruangan','ruangan.nama_ruangan','jam_kuliah.jam','kelas.nama_kelas')
                                        ->get();


                                  }


                                  elseif(empty($arr))
                                  {
                                    foreach($semua as $word)
                                    {


                                        $result .= '<tr>


                                                    <td>'.$word->nama_mk.'</td>
                                                    <td>'.$word->nama.'</td>
                                                    <td>'.$word->hari.'</td>
                                                    <td>'.$word->id.'</td>
                                                    <td><button class="btn btn-info btn-sm" onClick="tambah_krs(\''.$word->id.'\',\''.$word->kode_mk.'\',\''.$word->nidn.'\',\''.$word->semester.'\',\''.$word->kode_kelas.'\')"><i class="fas fa-plus-square"></i></button></td>
                                                    </tr>';
                                    }
                                  }









                          if(!empty($arr)){
                          foreach($semua as $word)
                          {

                            $itung = \DB::table('khs')
                                      ->where('id_jadwal',$word->id)
                                      ->select('id_jadwal')
                                      ->count();



                            $semuanya = DB::table('jadwal_kuliah')
                                        ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                                        ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                                        ->where('jadwal_kuliah.id',$word->id)
                                        ->get()->toArray();


                                        if(!empty($semuanya)){

                                        $result .= '<tr>
                                                    <td>'.$word->nama_mk.'</td>
                                                    <td>'.$word->nama.'</td>
                                                    <td>'.$word->hari.'</td>
                                                    <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                    <td>'.$semuanya[0]->jam.'/'.$semuanya[0]->nama_ruangan.'</td>
                                                    <td>'.$word->nama_kelas.'</td>
                                                    <td><button class="btn btn-info btn-sm" onClick="tambah_krs(\''.$word->id.'\',\''.$word->kode_mk.'\',\''.$word->nidn.'\',\''.$word->semester.'\',\''.$word->kode_kelas.'\')"><i class="fas fa-plus-square"></i> '.$itung.'/50</button></td>
                                                    </tr>';
                                                  }
                                                  else{
                                                    $result .= '<tr>
                                                                <td>'.$word->nama_mk.'</td>
                                                                <td>'.$word->nama.'</td>
                                                                <td>'.$word->hari.'</td>
                                                                <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                                <td>'.'-'.'</td>
                                                                <td>'.$word->nama_kelas.'</td>
                                                                <td><button class="btn btn-info btn-sm" onClick="tambah_krs(\''.$word->id.'\',\''.$word->kode_mk.'\',\''.$word->nidn.'\',\''.$word->semester.'\',\''.$word->kode_kelas.'\')"><i class="fas fa-plus-square"></i> '.$itung.'/50</button></td>
                                                                </tr>';

                                                  }
                          }
                          foreach($arr as $kurang)
                          {


                              $result .= '<tr>


                                          <td>'.$kurang->nama_mk.'</td>
                                          <td>'.$kurang->nama.'</td>
                                          <td>'.$kurang->hari.'</td>
                                          <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                          <td>'.'-'.'</td>
                                          <td>'.$word->nama_kelas.'</td>
                                          <td><span class="label warning" color="#FF0000">Kelas Penuh</span></td>
                                          </tr>';
                          }
                        }

                        elseif(empty($arr))
                        {

                          foreach($semua as $word)
                          {





                            $itung = \DB::table('khs')
                                      ->where('id_jadwal',$word->id)
                                      ->select('id_jadwal')
                                      ->count();



                            $semuanya = DB::table('jadwal_kuliah')
                                        ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                                        ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                                        ->where('jadwal_kuliah.id',$word->id)
                                        ->get()->toArray();



                              if(!empty($semuanya)){

                              $result .= '<tr>
                                          <td>'.$word->nama_mk.'</td>
                                          <td>'.$word->nama.'</td>
                                          <td>'.$word->hari.'</td>
                                          <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                          <td>'.$semuanya[0]->jam.'/'.$semuanya[0]->nama_ruangan.'</td>
                                          <td>'.$word->nama_kelas.'</td>
                                          <td><button class="btn btn-info btn-sm" onClick="tambah_krs(\''.$word->id.'\',\''.$word->kode_mk.'\',\''.$word->nidn.'\',\''.$word->semester.'\',\''.$word->kode_kelas.'\')"><i class="fas fa-plus-square"></i> '.$itung.'/50</button></td>
                                          </tr>';
                                        }
                                        else{
                                          $result .= '<tr>
                                                      <td>'.$word->nama_mk.'</td>
                                                      <td>'.$word->nama.'</td>
                                                      <td>'.$word->hari.'</td>
                                                      <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                      <td>'.'-'.'</td>
                                                      <td>'.$word->nama_kelas.'</td>
                                                      <td><button class="btn btn-info btn-sm" onClick="tambah_krs(\''.$word->id.'\',\''.$word->kode_mk.'\',\''.$word->nidn.'\',\''.$word->semester.'\',\''.$word->kode_kelas.'\')"><i class="fas fa-plus-square"></i> '.$itung.'/50</button></td>
                                                      </tr>';

                                        }
                          }
                        }





                          $result .='</table>';
                          return $result;
                }



    function hapusKrs(Request $request){

        $krs = Khs::find($request->id);
        $krs->delete();
    }

  function tambahKrs(Request $request){
        $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
        $dosen = \DB::table('dosen')->where('nidn');



        $khs = DB::table('khs')
              ->updateOrInsert([
                'kode_mk'           =>  $request->kode_mk,
                'id_jadwal'         =>  $request->id,
                'nilai_kehadiran'   =>  0,
                'nilai_tugas'       =>  0,
                'nilai_uts'         =>  0,
                'nilai_uas'         =>  0,
                'nidn'              =>  $request->nidn,
                'semester'          =>  $request->semester,
                'kode_kelas'        =>  $request->kode_kelas,
                'nim'               =>  Auth::guard('mahasiswa')->user()->nim,
                'kode_tahun_akademik' => $tahun_akademik->kode_tahun_akademik
              ]);

    }

    function tampilKrs(){
      $result = '<table class="table table-bordered">
              <tr><th width="230">Nama Matakuliah</th><th width="250">Nama Dosen</th><th width="80">hari</th><th width="130">Teori(Jam/Ruang)</th><th width="130">Praktikum(Jam/Ruang)</th><th width="130">Kelas</th><th>Action</th<tr>';

        $krs = \DB::table('khs')
                ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                ->join('jadwal_kuliah','khs.id_jadwal','=','jadwal_kuliah.id')
                ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                ->where('nim',Auth::guard('mahasiswa')->user()->nim)
                ->select('khs.kode_mk','matakuliah.nama_mk','dosen.nama','matakuliah.jml_sks','jadwal_kuliah.semester','khs.id','jadwal_kuliah.nidn','khs.id_jadwal','jam_kuliah.jam','jadwal_kuliah.hari','ruangan.kode_ruangan','ruangan.nama_ruangan','kelas.nama_kelas')
                ->get();









        foreach($krs as $row)
        {

          $praktikum = DB::table('jadwal_kuliah')
                      ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                      ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                      ->where('jadwal_kuliah.id',$row->id_jadwal)
                      ->get()->toArray();

            if(!empty($praktikum)){
            $result .= '<tr>

                        <td>'.$row->nama_mk.'</td>
                        <td>'.$row->nama.'</td>
                        <td>'.$row->hari.'</td>
                        <td>'.$row->jam.'/'.$row->nama_ruangan.'</td>
                        <td>'.$praktikum[0]->jam.'/'.$praktikum[0]->nama_ruangan.'</td>
                        <td>'.$row->nama_kelas.'</td>
                        <td><button class="btn btn-danger btn-sm" onClick="hapus_krs('.$row->id.')"><i class="fas fa-trash-alt"></i></button></td>
                        </tr>';
        }
        elseif(empty($praktikum)){
        $result .= '<tr>

                    <td>'.$row->nama_mk.'</td>
                    <td>'.$row->nama.'</td>
                    <td>'.$row->hari.'</td>
                    <td>'.$row->jam.'/'.$row->nama_ruangan.'</td>
                    <td>-</td>
                    <td>'.$row->nama_kelas.'</td>
                    <td><button class="btn btn-danger btn-sm" onClick="hapus_krs('.$row->id.')"><i class="fas fa-trash-alt"></i></button></td>
                    </tr>';
    }
      }

        $result .='</table>';
        return $result;


      }

    function index(){

      $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
      $nim = Auth::guard('mahasiswa')->user()->nim;

      $waktu = \DB::table('tahun_akademik')
              ->where('status','y')
              ->select('tahun_akademik.tanggal_awal_krs','tahun_akademik.tanggal_akhir_krs','kode_tahun_akademik')
              ->first();



      $date1 = strtotime("today");
      $date2 = strtotime($waktu->tanggal_awal_krs);
      $date3 = strtotime($waktu->tanggal_akhir_krs);

                  If ($date2 <= $date1 && $date3 >= $date1) {
                        return view('krs.index');
              }

              Else {
            return redirect('/mahasiswa/home')->with('status', 'Pengisian KRS Hanya Bisa Pada Waktu Yang Sudah Ditentukan. Terima Kasih');
              }

    }

    function daftarPengajuan(){

      $totalSKS=0;

      $result = '<table class="table table-bordered">
              <tr><th>Kode MK</th><th>Nama Matakuliah</th><th>SKS</th><th>Semester</th><th>Status</th><th width="20"></th><tr>';


      $nim = Auth::guard('mahasiswa')->user()->nim;
      $jurusan = Auth::guard('mahasiswa')->user()->kode_jurusan;
      $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();

              $semester = \DB::table('pengajuan')
              ->where('tahun_akademik',$tahun_akademik->kode_tahun_akademik)
              ->where('nim',$nim)
              ->get();


              foreach ($semester as $value) {

                  $data[] = $value->kode_mk;
                    }


                    if($tahun_akademik->kode_tahun_akademik % 2 == 0){
                          if(empty($data)){
                            $khs = \DB::table('khs')
                                    ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                                    ->join('dosen','khs.nidn','=','dosen.nidn')
                                    ->whereIn('semester', [2, 4, 6, 8])
                                    ->where('khs.nim',$nim)
                                    ->get();

                                    if(empty($khs)){
                                      $matakuliah = \DB::table('matakuliah')
                                              ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                              ->where('kode_jurusan',$jurusan)
                                              ->whereIn('semester', [2, 4, 6, 8])
                                              ->get();

                                    }
                                    else{
                                      foreach ($khs as $var) {
                                        $matakuliah = \DB::table('matakuliah')
                                                ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                                ->where('kode_jurusan',$jurusan)
                                                ->whereNotIn('matakuliah.kode_mk',[$var->kode_mk])
                                                ->whereIn('semester', [2, 4, 6, 8])
                                                ->get();
                                              }

                                    }
                                }



                        else{//Else jika $data kosong
                              $khs = \DB::table('khs')
                                      ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                                      ->join('dosen','khs.nidn','=','dosen.nidn')
                                      ->where('khs.nim',$nim)
                                      ->whereIn('semester', [2, 4, 6, 8])
                                      ->whereNotIn('khs.kode_mk',$data)
                                      ->get();

                                      if(empty($khs)){
                                        $matakuliah = \DB::table('matakuliah')
                                                ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                                ->where('kode_jurusan',$jurusan)
                                                ->whereNotIn('khs.kode_mk',$data)
                                                ->whereIn('semester', [2, 4, 6, 8])
                                                ->get();

                                      }
                                      else{
                                        foreach ($khs as $var) {
                                          $matakuliah = \DB::table('matakuliah')
                                                  ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                                  ->where('kode_jurusan',$jurusan)
                                                  ->whereNotIn('khs.kode_mk',$data)
                                                  ->whereNotIn('matakuliah.kode_mk',[$var->kode_mk])
                                                  ->whereIn('semester', [2, 4, 6, 8])
                                                  ->get();
                                                }

                                      }


                                            }//Tutup else data kosong



                                }//Tutup If ganjil/genap

                  else{ //Else Ganjil Genap
                    if(empty($data)){
                      $khs = \DB::table('khs')
                              ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                              ->join('dosen','khs.nidn','=','dosen.nidn')
                              ->whereIn('semester', [1, 3, 5, 7])
                              ->where('khs.nim',$nim)
                              ->get();





                              if(empty($khs)){
                                $matakuliah = \DB::table('matakuliah')
                                        ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                        ->where('kode_jurusan',$jurusan)
                                        ->whereIn('semester', [1, 3, 5, 7])
                                        ->get();

                              }
                              else{


                                foreach ($khs as $var) {
                                  $values[] = $var->kode_mk;
                                }
                                if(empty($values)){
                                  $matakuliah = \DB::table('matakuliah')
                                          ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                          ->where('kode_jurusan',$jurusan)
                                          ->whereIn('semester', [1, 3, 5, 7])
                                          ->get();
                                }
                                  else{
                                  $matakuliah = \DB::table('matakuliah')
                                          ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                          ->where('kode_jurusan',$jurusan)
                                          ->whereNotIn('matakuliah.kode_mk',$values)
                                          ->whereIn('semester', [1, 3, 5, 7])
                                          ->get();
                                        }
                                    }







                              }

                               // tutup if data kosong
                            else{//else data kosong
                                $khs = \DB::table('khs')
                                        ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                                        ->join('dosen','khs.nidn','=','dosen.nidn')
                                        ->where('khs.nim',$nim)
                                        ->whereIn('semester', [1, 3, 5, 7])
                                        ->whereNotIn('khs.kode_mk',$data)
                                        ->get();

                                        if(empty($khs)){


                                          $matakuliah = \DB::table('matakuliah')
                                                  ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                                  ->where('kode_jurusan',$jurusan)
                                                  ->whereNotIn('matakuliah.kode_mk',$data)
                                                  ->whereIn('semester', [1, 3, 5, 7])
                                                  ->get();

                                        }
                                        else{
                                          foreach ($khs as $var) {
                                            $values[] = $var->kode_mk;
                                          }

                                          if(empty($values)){
                                            $matakuliah = \DB::table('matakuliah')
                                                    ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                                    ->where('kode_jurusan',$jurusan)
                                                    ->whereNotIn('matakuliah.kode_mk',$data)
                                                    ->whereIn('semester', [1, 3, 5, 7])
                                                    ->get();
                                          }

                                          else{


                                            $matakuliah = \DB::table('matakuliah')
                                                    ->join('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                                    ->where('kode_jurusan',$jurusan)
                                                    ->whereNotIn('matakuliah.kode_mk',$data)
                                                    ->whereNotIn('matakuliah.kode_mk',$values)
                                                    ->whereIn('semester', [1, 3, 5, 7])
                                                    ->get();
                                                  }
                                              }



                                      }//Tutup else data kosong
                          } //tutup else ganjil-genap


                foreach ($khs as $key) {
                    $nilai = hitung_nilai($key->id);
                        if($nilai < 60)
                        {
                            $tampil = \DB::table('khs')
                                  ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                                  ->leftJoin('kurikulum','khs.kode_mk','=','kurikulum.kode_mk')
                                  ->where('khs.id',$key->id)
                                  ->where('nim',$nim)
                                  ->select('khs.kode_mk','matakuliah.nama_mk','matakuliah.jml_sks','kurikulum.semester')
                                  ->get();



                            $arr[] = $tampil[0];
                          }
                    }


                      foreach ($matakuliah as $row) {

                        $semua = \DB::table('matakuliah')
                              ->leftJoin('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                              ->where('matakuliah.kode_mk',$row->kode_mk)
                              // ->whereNotIn('matakuliah.kode_mk',['TI003'])
                              ->where('kode_jurusan',$jurusan)
                              ->get();

                              $array[] = $semua[0];
                      }






                      $jumlahsks = \DB::table('pengajuan')
                          ->join('matakuliah','pengajuan.kode_mk','=','matakuliah.kode_mk')
                          ->where('nim',Auth::guard('mahasiswa')->user()->nim)
                          ->get();

                          foreach ($jumlahsks as $key) {
                            $totalSKS=$totalSKS+$key->jml_sks;
                          }


                        if($totalSKS >= 24){
                          $result .= '<tr>
                                      <td><span class="label warning" color="#FF0000">SKS Sudah Maksimal</span><td>
                                      </tr>';
                        }
                        elseif ($totalSKS == 21) {
                          $matkulmax = \DB::table('matakuliah')
                                ->leftJoin('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                ->where('matakuliah.kode_mk',$row->kode_mk)
                                ->where('kode_jurusan',$jurusan)
                                ->where('jml_sks',[2,3])
                                ->get();

                                foreach($matkulmax as $word)
                                {
                                    $result .= '<tr>

                                                <td>'.$word->kode_mk.'</td>
                                                <td>'.$word->nama_mk.'</td>
                                                <td>'.$word->jml_sks.'</td>
                                                <td>'.$word->semester.'</td>
                                                <td><button class="btn btn-info btn-sm" onClick="tambah_pengajuan(\''.$word->kode_mk.'\',\''.$word->semester.'\')"><i class="fas fa-plus-square"></i></button></td>
                                                </tr>';
                                }
                        }
                        elseif ($totalSKS == 22) {
                          $matkulmax = \DB::table('matakuliah')
                                ->leftJoin('kurikulum','matakuliah.kode_mk','=','kurikulum.kode_mk')
                                ->where('matakuliah.kode_mk',$row->kode_mk)
                                // ->whereNotIn('matakuliah.kode_mk',['TI003'])
                                ->where('kode_jurusan',$jurusan)
                                ->where('jml_sks',2)
                                ->get();

                                foreach($matkulmax as $word)
                                {
                                    $result .= '<tr>

                                                <td>'.$word->kode_mk.'</td>
                                                <td>'.$word->nama_mk.'</td>
                                                <td>'.$word->jml_sks.'</td>
                                                <td>'.$word->semester.'</td>
                                                <td><button class="btn btn-info btn-sm" onClick="tambah_pengajuan(\''.$word->kode_mk.'\',\''.$word->semester.'\')"><i class="fas fa-plus-square"></i></button></td>
                                                </tr>';
                                }
                        }

                        else{

                      if(empty($arr)){
                        foreach($matakuliah as $word)
                        {
                            $result .= '<tr>

                                        <td>'.$word->kode_mk.'</td>
                                        <td>'.$word->nama_mk.'</td>
                                        <td>'.$word->jml_sks.'</td>
                                        <td>'.$word->semester.'</td>
                                        <td>BARU</td>
                                        <td><button class="btn btn-info btn-sm" onClick="tambah_pengajuan(\''.$word->kode_mk.'\',\''.$word->semester.'\')"><i class="fas fa-plus-square"></i></button></td>
                                        </tr>';
                        }



                      }
                      elseif(empty($array)){

                        foreach($arr as $word)
                        {

                            $result .= '<tr>

                                        <td>'.$word->kode_mk.'</td>
                                        <td>'.$word->nama_mk.'</td>
                                        <td>'.$word->jml_sks.'</td>
                                        <td>'.$word->semester.'</td>
                                        <td>ULANG</td>
                                        <td><button class="btn btn-info btn-sm" onClick="tambah_pengajuan(\''.$word->kode_mk.'\',\''.$word->semester.'\')"><i class="fas fa-plus-square"></i></button></td>
                                        </tr>';
                        }

                      }


                      else{

                      $hasil = (array_merge($arr,$array));

                      foreach($arr as $word)
                      {

                          $result .= '<tr>

                                      <td>'.$word->kode_mk.'</td>
                                      <td>'.$word->nama_mk.'</td>
                                      <td>'.$word->jml_sks.'</td>
                                      <td>'.$word->semester.'</td>
                                      <td>ULANG</td>
                                      <td><button class="btn btn-info btn-sm" onClick="tambah_pengajuan(\''.$word->kode_mk.'\',\''.$word->semester.'\')"><i class="fas fa-plus-square"></i></button></td>
                                      </tr>';
                      }
                      foreach($array as $word)
                      {

                          $result .= '<tr>

                                      <td>'.$word->kode_mk.'</td>
                                      <td>'.$word->nama_mk.'</td>
                                      <td>'.$word->jml_sks.'</td>
                                      <td>'.$word->semester.'</td>
                                      <td>BARU</td>
                                      <td><button class="btn btn-info btn-sm" onClick="tambah_pengajuan(\''.$word->kode_mk.'\',\''.$word->semester.'\')"><i class="fas fa-plus-square"></i></button></td>
                                      </tr>';
                      }
                    }

                  }




      $result .='</table>';


      return $result;
    }

      function indexPengajuan(){

      $waktu = \DB::table('tahun_akademik')
              ->where('status','y')
              ->select('tahun_akademik.tanggal_awal_krs','tahun_akademik.tanggal_akhir_krs','tahun_akademik.tanggal_awal_pengajuan','tahun_akademik.tanggal_akhir_pengajuan')
              ->first();

      $data['pengajuan'] = \DB::table('pengajuan')
              ->join('matakuliah','pengajuan.kode_mk','=','matakuliah.kode_mk')
              ->where('nim',Auth::guard('mahasiswa')->user()->nim)
              ->get();





      $date1 = strtotime("today");
      $date2 = strtotime($waktu->tanggal_awal_pengajuan);
      $date3 = strtotime($waktu->tanggal_akhir_pengajuan);

                  If ($date2 <= $date1 && $date3 >= $date1) {

                    return view('krs.pengajuan',$data);
                  }


                Else {
                return redirect('/mahasiswa/home')->with('status', 'Pengajuan Matakuliah Hanya Bisa Pada Waktu Yang Sudah Ditentukan. Terima Kasih');
                }
}

    function hapusPengajuan(Request $request){
        $pengajuan = Pengajuan::find($request->id);
        $pengajuan->delete();
    }

  function tambahPengajuan(Request $request){
          $totalSKS=0;

                $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
                $dosen = \DB::table('dosen')->where('nidn');



                $pengajuan = DB::table('pengajuan')
                  ->updateOrInsert([
                    'kode_mk'           =>  $request->kode_mk,
                    'semester'          =>  $request->semester,
                    'nim'               =>  Auth::guard('mahasiswa')->user()->nim,
                    'tahun_akademik' => $tahun_akademik->kode_tahun_akademik
                  ]);

    }

    function tampilPengajuan(){

        $totalSKS=0;
        $result = '<table class="table table-bordered">
                <tr><th>Kode MK</th><th>Nama Matakuliah</th><th>SKS</th><th>Semester</th><th width="20"></th><tr>';

        $pengajuan = \DB::table('pengajuan')
                ->join('matakuliah','pengajuan.kode_mk','=','matakuliah.kode_mk')
                ->where('nim',Auth::guard('mahasiswa')->user()->nim)
                ->get();


                    foreach ($pengajuan as $key) {
                      $totalSKS=$totalSKS+$key->jml_sks;
                    }


        foreach($pengajuan as $row)
        {
            $result .= '<tr>

                        <td>'.$row->kode_mk.'</td>
                        <td>'.$row->nama_mk.'</td>
                        <td>'.$row->jml_sks.'</td>
                        <td>'.$row->semester.'</td>
                        <td><button class="btn btn-danger btn-sm" onClick="hapus_pengajuan('.$row->id.')"><i class="fas fa-trash-alt"></i></button></td>
                        </tr>';

        }





        $result .='</table>';

        echo('sks yang diambil '.$totalSKS.' dari 24 SKS');



        return $result;


      }

      function CetakPdf(){
        $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
        $nim = Auth::guard('mahasiswa')->user()->nim;
        $nama = Auth::guard('mahasiswa')->user()->nama_mahasiswa;
        $setting    = \DB::table('setting')->where('id',1)->first();

        $mahasiswa = \DB::table('mahasiswa')
                    // ->join('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                    ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                    ->leftJoin('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                    ->leftJoin('dosen','kelas.nidn','=','dosen.nidn')
                    ->where('mahasiswa.nim',$nim)
                    ->first();




        $jurusan = \DB::table('mahasiswa')
                    ->leftJoin('jurusan','mahasiswa.kode_jurusan','=','jurusan.kode_jurusan')
                    ->leftJoin('dosen','jurusan.nidn','=','dosen.nidn')
                    ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                    ->where('mahasiswa.nim',$nim)
                    ->first();

        $cetakpdf = DB::table('khs')
                ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
                ->join('dosen','khs.nidn','=','dosen.nidn')
                ->join('kelas','khs.kode_kelas','=','kelas.kode_kelas')
                ->where('kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
                ->where('nim',$nim)
                ->get();

                Fpdf::AddPage();
                Fpdf::SetFont('Arial', 'B', 18);
                Fpdf::Image('https://4.bp.blogspot.com/-nOCPZVY8wdU/XJeti2m55eI/AAAAAAAAAQU/ih5ivB83uLM1IqXEX5OtzjBQdZ-rp_N5QCLcBGAs/s1600/aasasa.png',10,5,-1000);
                Fpdf::Cell(185,7,$setting->nama_universitas,0,1,'C');

                Fpdf::SetFont('Arial', '', 12);
                Fpdf::Cell(185,5,$setting->alamat,0,1,'C');
                Fpdf::SetFont('Arial', '', 12);
                Fpdf::Cell(185,5,"Telpon : ".$setting->no_telpon." | Email : ".$setting->email.' | Fax : '.$setting->fax,0,1,'C');

                Fpdf::Line(10,30,190,30);
                Fpdf::Line(10,31,190,31);

                Fpdf::Cell(30,10,'',0,1);
                Fpdf::SetFont('Arial', 'B', 14);
                Fpdf::Cell(185,5,'Kartu Rencana Studi',0,0,'C');

                Fpdf::Cell(30,10,'',0,1);
                // biodata mahasiswa
                Fpdf::SetFont('Arial', '', 12);
                Fpdf::Cell(35,5,'NIM',0,0);
                Fpdf::Cell(50,5,' : '.$mahasiswa->nim,0,0);
                Fpdf::Cell(10,5,'',0,0);
                Fpdf::Cell(35,5,'Jurusan',0,0);
                Fpdf::Cell(10,5,' : '.$jurusan->nama_jurusan,0,1);

                Fpdf::Cell(35,5,'Nama',0,0);
                Fpdf::Cell(50,5,' : '.$mahasiswa->nama_mahasiswa,0,0);
                Fpdf::Cell(10,5,'',0,0);
                if(!empty($info)){
                Fpdf::Cell(35,5,'Semester Tempuh',0,0);
                Fpdf::Cell(10,5,' : '.$info->semester,0,1);
              }
              if(empty($info)){
              Fpdf::Cell(35,5,'Semester Tempuh',0,0);
              Fpdf::Cell(10,5,' : '.'1',0,1);
            }

                Fpdf::Cell(35,5,'Tahun Akademik',0,0);
                Fpdf::Cell(50,5,' : '.$tahun_akademik->tahun_akademik,0,0);
                Fpdf::Cell(10,5,'',0,0);


                Fpdf::Cell(30,10,'',0,1);


                Fpdf::Cell(10,7,'No',1,0);
                Fpdf::Cell(25,7,'KODE MK',1,0);
                Fpdf::Cell(50,7,'NAMA MK',1,0);
                Fpdf::Cell(50,7,'DOSEN',1,0);
                Fpdf::Cell(30,7,'KELAS',1,0);
                Fpdf::Cell(25,7,'Semester',1,1);

                Fpdf::SetFont('Times', '', 11);
                $no         =1;

                foreach($cetakpdf as $key)
                {
                    Fpdf::Cell(10,7,$no,1,0);
                    Fpdf::Cell(25,7,$key->kode_mk,1,0);
                    Fpdf::Cell(50,7,$key->nama_mk,1,0);
                    Fpdf::Cell(50,7,$key->nama,1,0);
                    Fpdf::Cell(30,7,$key->nama_kelas,1,0);
                    Fpdf::Cell(25,7,$key->semester,1,1);



                }

                Fpdf::Cell(30,10,'',0,1);
                Fpdf::Cell(65,10,'Mahasiswa',0,0);
                Fpdf::Cell(65,10,'Pembimbing Akademik',0,0);
                Fpdf::Cell(35,7,'Kaprodi '.$jurusan->nama_jurusan,0,1);
                Fpdf::Cell(100,20,'',0,1);
                Fpdf::Cell(1,10,'',0,0);

                Fpdf::Cell(65,10,$mahasiswa->nama_mahasiswa,0,0);
                Fpdf::Cell(60,7,$mahasiswa->nama,0,0);
                Fpdf::Cell(35,7,$jurusan->nama,0,1);

                Fpdf::Output('I',"KRS_$nama");
                exit();
      }

    function cetak(){
      $nim = Auth::guard('mahasiswa')->user()->nim;
      $tahun_akademik = \DB::table('tahun_akademik')->where('status','y')->first();
      $cetak = DB::table('khs')
              ->join('matakuliah','khs.kode_mk','=','matakuliah.kode_mk')
              ->join('dosen','khs.nidn','=','dosen.nidn')
              ->join('kelas','khs.kode_kelas','=','kelas.kode_kelas')
              ->where('kode_tahun_akademik',$tahun_akademik->kode_tahun_akademik)
              ->where('nim',$nim)
              ->get();
          return Datatables::of($cetak)
          ->make(true);

    }
    function IndexCetak(){
        return view('krs.cetak');

    }


}
