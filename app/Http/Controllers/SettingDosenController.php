<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Dosen;
use Auth;


class SettingDosenController extends Controller
{
    function index(){
        $nidn = Auth::guard('dosen')->user()->nidn;
        $data['settingdosen'] = Dosen::find($nidn);
        // ,'=',$nim)->first();
        return view('settingdosen',$data);
    }

    function update (Request $request){
      $request->validate([
          'tempat_lahir' => 'required|min:5',
          'tanggal_lahir' => 'required|min:6',
          'email'     =>'required|unique:dosen',
          'alamat'     =>'required',
          'no_hp'     =>'required'
      ]);

        $ubahnidn = Auth::guard('dosen')->user()->nidn;
        $setting = Dosen::find($ubahnidn);
        $setting->nama = $request->nama;
        $setting->email = $request->email;
        $setting->no_hp = $request->no_hp;
        $setting->alamat = $request->alamat;
        $setting->tempat_lahir = $request->tempat_lahir;
        $setting->tanggal_lahir = $request->tanggal_lahir;

        if($request->password!='')
        {
            $setting->password = $request->password;
        }

        $setting->save();


        return redirect()->back()->with('status','Perubahan Berhasil Disimpan');
    }
}
