<?php

namespace App\Http\Controllers;
use App\Kehadiran;
use Auth;

use Illuminate\Http\Request;

class KehadiranController extends Controller
{
    function index($id_jadwal){

        $tahunaktif = \DB::table('tahun_akademik')
                      ->where('tahun_akademik.status','y')->first();

                      $dosen = Auth::guard('dosen')->user()->nidn;


        $jadwal = \DB::table('jadwal_kuliah')
                ->join('dosen','dosen.nidn','=','jadwal_kuliah.nidn')
                ->join('matakuliah','matakuliah.kode_mk','=','jadwal_kuliah.kode_mk')
                ->where('jadwal_kuliah.nidn',$dosen)
                ->where('kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
                ->where('id',$id_jadwal)->first();

                
                

        $data['mahasiswa'] = \DB::table('khs')
                            ->join('mahasiswa','mahasiswa.nim','=','khs.nim')
                            ->where('khs.nidn',$jadwal->nidn)
                            ->where('khs.kode_mk',$jadwal->kode_mk)
                            ->get();
        $data['jadwal']    = $jadwal;
        return view('kehadiran.index',$data);

    }

    function create($id_jadwal)
    {
        $jadwal = \DB::table('jadwal_kuliah')
                ->join('dosen','dosen.nidn','=','jadwal_kuliah.nidn')
                ->join('matakuliah','matakuliah.kode_mk','=','jadwal_kuliah.kode_mk')
                ->where('id',$id_jadwal)->first();

        $pertemuan = \DB::table('kehadiran')
                    ->where('kode_mk',$jadwal->kode_mk)
                    ->where('nidn',$jadwal->nidn)
                    ->where('kode_tahun_akademik',$jadwal->kode_tahun_akademik)
                    ->where('kode_jurusan',$jadwal->kode_jurusan)->count();


        $data['jadwal']    = $jadwal;
        $data['pertemuan_ke'] = $pertemuan+1;
        return view('kehadiran.create',$data);
    }

    function store(Request $request,$id_jadwal){

        $jadwal = \DB::table('jadwal_kuliah')
        
        ->where('id',$id_jadwal)
        ->first();

        $kehadiran = new Kehadiran;
        $kehadiran->kode_mk             = $jadwal->kode_mk;
        $kehadiran->nidn          = $jadwal->nidn;
        $kehadiran->kode_jurusan        = $jadwal->kode_jurusan;
        $kehadiran->kode_ruang          = $jadwal->kode_ruangan;
        $kehadiran->kode_tahun_akademik = $jadwal->kode_tahun_akademik;
        $kehadiran->topik_pembahasan    = $request->topik;
        $kehadiran->pertemuan_ke        = $request->pertemuan_ke;
        $kehadiran->save();

        $lastID = $kehadiran->id;
        $pertemuan_ke = $kehadiran->pertemuan_ke;

        return redirect('kehadiran/'.$lastID.'/absen/');

    }


    function show($id_kehadiran)
    {
        $kehadiran          = \DB::table('kehadiran')
                            ->join('dosen','dosen.nidn','=','kehadiran.nidn')
                            ->join('matakuliah','matakuliah.kode_mk','=','kehadiran.kode_mk')
                            ->where('kehadiran.id',$id_kehadiran)
                            ->first();
                            

        $data['mahasiswa'] = \DB::table('khs')
                            ->join('mahasiswa','mahasiswa.nim','=','khs.nim')
                            ->where('khs.nidn',$kehadiran->nidn)
                            ->where('khs.kode_mk',$kehadiran->kode_mk)
                            ->get();
        $data['kehadiran'] = $kehadiran;
        return view('kehadiran.show',$data);
    }

    function update(Request $request)
    {
        $chek = \DB::table('riwayat_kehadiran')
        ->where('nim',$request->nim)
        ->where('kehadiran_id',$request->id_kehadiran)
        ->count();
        if($chek>0)
        {
            // lakukan update

            \DB::table('riwayat_kehadiran')
                    ->where('kehadiran_id',$request->id_kehadiran)
                    ->update([
                    'status_kehadiran'=>$request->status_kehadiran
                    ]);
        }else{

            $riwayat = \DB::table('kehadiran')
                    ->select('kehadiran.pertemuan_ke')
                    ->where('kehadiran.id',$request->id_kehadiran)
                    ->first();

                    // dd($riwayat);
            // lakukan insert
            \DB::table('riwayat_kehadiran')
                    ->insert([
                            'nim'=>$request->nim,
                            'kehadiran_id'=>$request->id_kehadiran,
                            'status_kehadiran'=>$request->status_kehadiran,
                            'pertemuan_ke' => $riwayat->pertemuan_ke
                            ]);
        }
        
    }
}
