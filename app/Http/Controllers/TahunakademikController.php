<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TahunAkademik;
use DataTables;
use DateTime;


class TahunakademikController extends Controller
{
    function json(){
        return Datatables::of(TahunAkademik::all())
        ->addColumn('action', function ($row) {
              return view('modal.akademik' , [
             'model' => $row,
               'url_edit' => route('tahunakademik.edit', $row->kode_tahun_akademik),
               'url_destroy' => route('tahunakademik.destroy', $row->kode_tahun_akademik)
          ]);
      })
        ->addColumn('status', function ($row) {
            return $row->status=='y'?'Aktif':'Tidak Aktif';
        })
        ->addColumn('periode_perkuliahan', function ($row) {
            $awal = DateTime::createFromFormat('Y-m-d', $row->tanggal_awal_kuliah);
            $akhir = DateTime::createFromFormat('Y-m-d', $row->tanggal_akhir_kuliah);

            return $awal->format('d-M-Y').' - '.$akhir->format('d-M-Y');
            //return $row->tanggal_awal_kuliah.' - '.$row->tanggal_akhir_kuliah;
        })
        ->addColumn('periode_krs', function ($row) {
            $awal = DateTime::createFromFormat('Y-m-d', $row->tanggal_awal_krs);
            $akhir = DateTime::createFromFormat('Y-m-d', $row->tanggal_akhir_krs);

            return $awal->format('d-M-Y').' - '.$akhir->format('d-M-Y');
        })
        ->addColumn('periode_pengajuan', function ($row) {
            $awal = DateTime::createFromFormat('Y-m-d', $row->tanggal_awal_pengajuan);
            $akhir = DateTime::createFromFormat('Y-m-d', $row->tanggal_akhir_pengajuan);

            return $awal->format('d-M-Y').' - '.$akhir->format('d-M-Y');
        })
        ->rawColumns(['action'])
        ->make(true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tahun_akademik.index_update');
        //return view('tahun_akademik.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tahun_akademik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_tahun_akademik' => 'required|unique:tahun_akademik|min:5',
            'tahun_akademik' => 'required|min:5',
            'tanggal_awal_kuliah' => 'required|min:5',
            'tanggal_awal_pengajuan' => 'required|min:5',
            'tanggal_awal_krs' => 'required|min:5',
            'tanggal_akhir_krs' => 'required|min:5',
            'tanggal_akhir_pengajuan' => 'required|min:5',
            'tanggal_akhir_kuliah' => 'required|min:5'
        ]);

        if($request->status=='y')
        {
          $tahun_akademik_aktif = \DB::table('tahun_akademik')->where('status','y')->count();


          if($tahun_akademik_aktif >= 1)
          {


            $tahunaktif = TahunAkademik::where('status','=','y')->first();
            $key = 'n';

            $tahunaktif->status = $key;
            $tahunaktif->save();



            $tahun_akademik = New tahunAkademik();
            $tahun_akademik->create($request->all());
            return redirect('/tahunakademik')->with('status','Data tahun akademik Berhasil Disimpan');

          }
          else{
            $tahun_akademik = New tahunAkademik();
            $tahun_akademik->create($request->all());
            return redirect('/tahunakademik')->with('status','Data tahun akademik Berhasil Disimpan');
          }
        }



    }

    function tahun_aktif(){


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_tahun_akademik)
    {
        $data['tahunakademik'] = TahunAkademik::where('kode_tahun_akademik',$kode_tahun_akademik)->first();
        return view('tahun_akademik.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_tahun_akademik)
    {



        $request->validate([
            'tahun_akademik' => 'required|min:5'
        ]);


        $tahun_akademik = TahunAkademik::where('kode_tahun_akademik','=',$kode_tahun_akademik);


        $tahun_akademik->update($request->except('_method','_token'));

        $tahun_akademik_aktif = \DB::table('tahun_akademik')->where('status','y')->first();
        if(empty($tahun_akademik)){
          return view('tahun_akademik.create');
        }
        else{
        return redirect('/tahunakademik')->with('status','Data tahun_akademik Berhasil Di Update');;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_tahun_akademik)
    {
        $tahun_akademik = TahunAkademik::where('kode_tahun_akademik',$kode_tahun_akademik);
        $tahun_akademik->delete();
        return redirect('/tahunakademik')->with('status','Data tahun_akademik Berhasil Dihapus');;
    }
}
