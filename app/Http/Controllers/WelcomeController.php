<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
  function index(){
    if (Auth::guard('mahasiswa')->check()) {
            return redirect ('/mahasiswa/home');
          }
          if (Auth::guard('dosen')->check()) {
                  return redirect ('/jadwal_mengajar');
                }
                if (Auth::guard('web')->check()) {
                        return redirect ('/home');
                      }
                              else{
                          return view('welcome');
    }
  }
}
