<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kelompok;
use DataTables;

class kelompokController extends Controller
{
    function json(){
        return Datatables::of(Kelompok::all())
        ->addColumn('action', function ($row) {

              return view('modal.kelompok' , [
             'model' => $row,
               'url_edit' => route('kelompok.edit', $row->kode_kelompok),
               'url_destroy' => route('kelompok.destroy', $row->kode_kelompok)
          ]);
      })
      ->rawColumns(['action'])
      ->make(true);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kelompok.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Kelompok.create');
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_kelompok' => 'required|unique:kelompok|min:4',
            'nama_kelompok' => 'required|min:6'
        ]);


        $kelompok = New kelompok();
        $kelompok->create($request->all());
        return redirect('/kelompok')->with('status','Data kelompok Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode_kelompok)
    {
        $data['kelompok'] = kelompok::where('kode_kelompok',$kode_kelompok)->first();
        return view('kelompok.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode_kelompok)
    {
        $request->validate([
            'nama_kelompok' => 'required|min:6'
        ]);


        $kelompok = kelompok::where('kode_kelompok','=',$kode_kelompok);
        $kelompok->update($request->except('_method','_token'));
        return redirect('/kelompok')->with('status','Data kelompok Berhasil Di Update');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_kelompok)
    {
        $kelompok = kelompok::where('kode_kelompok',$kode_kelompok);
        $kelompok->delete();
        return redirect('/kelompok')->with('status','Data kelompok Berhasil Dihapus');;
    }
}
