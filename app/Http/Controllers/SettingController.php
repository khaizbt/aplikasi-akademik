<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Setting;

class SettingController extends Controller
{
    function index(){
        $data['setting'] = Setting::find(1);
        return view('setting',$data);
    }

    function update(Request $request){
        $setting = Setting::find(1);
        $setting->nama_universitas = $request->nama_universitas;
        $setting->fax = $request->fax;
        $setting->no_telpon = $request->no_telpon;
        $setting->web = $request->web;
        $setting->alamat = $request->alamat;
        $setting->save();


        return redirect()->back()->with('status','Perubahan Berhasil Dihapus');
    }
}
