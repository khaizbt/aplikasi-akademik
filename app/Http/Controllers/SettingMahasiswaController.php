<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use Illuminate\Http\Request;
use Auth;

class SettingMahasiswaController extends Controller
{
    function index(){
        $nim = Auth::guard('mahasiswa')->user()->nim;
        $data['settingmahasiswa'] = Mahasiswa::find($nim);
        // ,'=',$nim)->first();
        return view('settingmahasiswa',$data);
    }

    function update (Request $request){
      $request->validate([
          'email' => 'required|min:6',
          // 'password' => 'required|min:6',
          'tempat_lahir' => ' required|min:6',
          'tanggal_lahir' => ' required|min:6',
          'alamat' => ' required|min:6',
          'no_hp' => ' required|min:8',

      ]);


        $ubahnim = Auth::guard('mahasiswa')->user()->nim;
        $setting = Mahasiswa::find($ubahnim);

        $setting->email = $request->email;
        $setting->no_hp = $request->no_hp;
        $setting->alamat = $request->alamat;
        $setting->tempat_lahir = $request->tempat_lahir;
        $setting->tanggal_lahir = $request->tanggal_lahir;

        if($request->password!='')
        {
            $setting->password = $request->password;
        }

        $setting->save();
        return redirect()->back()->with('status','Perubahan Berhasil Disimpan');
    }
}
