<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class SettingAdminController extends Controller
{
    function index(){
        $nim = Auth::guard('web')->user()->id;
        $data['settingadmin'] = User::find($nim);


        // ,'=',$nim)->first();
        return view('settingadmin',$data);
    }

    function update (Request $request){

      $request->validate([
          'name' => 'required|min:4',
          'email'     =>'required|unique:dosen',
          ]);
          
        $ubahnim = Auth::guard('web')->user()->id;
        $setting = User::find($ubahnim);
        $setting->name = $request->name;
        $setting->email = $request->email;


        if($request->password!='')
        {
            $setting->password = $request->password;
        }
        $setting->save();
        return redirect()->back()->with('status','Perubahan Berhasil Disimpan');
    }
}
