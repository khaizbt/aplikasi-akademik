<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LoginDosenController extends Controller
{
    // menampilkan form login
    function index(){
      if (Auth::guard('mahasiswa')->check()) {
              return redirect ('/mahasiswa/home');
            }
            if (Auth::guard('dosen')->check()) {
                    return redirect ('/dosen/home');
                  }
                  if (Auth::guard('web')->check()) {
                          return redirect ('/home');
                        }
                                else{

                                    return view('dosen.login');
    }                             }

    // chek proses login
    function submit(Request $request)
    {


      // Attempt to log the user in
      if (Auth::guard('dosen')->attempt(['nidn' => $request->nidn, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended('/dosen/home');
      }
      elseif (Auth::guard('dosen')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended('/dosen/home');
      }
      // if unsuccessful, then redirect back to the login with the form data
      return redirect('dosen/login')->with('status','NIDN atau Password Salah,Login DITOLAK ')->withInput($request->only('nidn', 'remember'));
    }
}
