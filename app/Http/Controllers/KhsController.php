<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Fpdf;

class KhsController extends Controller
{
    function index()
    {
        $nim = Auth::guard('mahasiswa')->user()->nim;
        $data['khs'] = \DB::table('khs')
                ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                // ->join('dosen','dosen.nidn','=','khs.nidn')
                ->where('nim',$nim)
                ->get();



        return view('khs.transkrip_nilai',$data);
    }

    public function KHSSemester(Request $request)
    {
      $semester = $request->get('semester');//Diambil dari sini
      $nim = Auth::guard('mahasiswa')->user()->nim;
      $data['mahasiswa'] = \DB::table('khs')
                  ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                  // ->join('dosen','dosen.nidn','=','khs.nidn')
                  ->where('khs.nim',$nim)
                  ->where('khs.semester',$semester)
                  ->get();

    $data['semester_terpilih'] = $semester;
      return view('khs.khs_semester',$data);
    }

    public function SemesterPdf(Request $request)
    {
    // $this->KHSSemester($request);
      $semester = $request->get('semester');

      $nim = Auth::guard('mahasiswa')->user()->nim;
      $mahasiswa = \DB::table('mahasiswa')
                  ->join('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                  ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                  ->join('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                  ->join('dosen','jurusan.nidn','=','dosen.nidn')
                  ->where('mahasiswa.nim',$nim)
                  ->select('mahasiswa.nim','mahasiswa.nama_mahasiswa','tahun_akademik.tahun_akademik','jurusan.nama_jurusan','dosen.nama')
                  ->first();


      $dosbing     = \DB::table('mahasiswa')
                    ->join('kelas','mahasiswa.kode_kelas','kelas.kode_kelas')
                    ->join('dosen','kelas.nidn','dosen.nidn')
                    ->first();

      $setting    = \DB::table('setting')->where('id',1)->first();

      $khs = \DB::table('khs')
                  ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                  // ->join('dosen','dosen.nidn','=','khs.nidn')
                  ->where('khs.nim',$nim)
                  ->where('khs.semester',$semester)
                  ->get();

        $semester_mahasiswa = \DB::table('khs')
              ->where('nim',$nim)
              ->where('khs.semester',$semester)
              ->first();
                if($khs->count()){

                  Fpdf::AddPage();
                  Fpdf::SetFont('Arial', 'B', 18);
                  Fpdf::Image('https://4.bp.blogspot.com/-nOCPZVY8wdU/XJeti2m55eI/AAAAAAAAAQU/ih5ivB83uLM1IqXEX5OtzjBQdZ-rp_N5QCLcBGAs/s1600/aasasa.png',10,5,-1000);
                  Fpdf::Cell(185,7,$setting->nama_universitas,0,1,'C');

                  Fpdf::SetFont('Arial', '', 12);
                  Fpdf::Cell(185,5,$setting->alamat,0,1,'C');
                  Fpdf::SetFont('Arial', '', 12);
                  Fpdf::Cell(185,5,"Tlp : ".$setting->no_telpon." | Email : ".$setting->email.' | Fax : '.$setting->fax,0,1,'C');

                  Fpdf::Line(10,30,190,30);
                  Fpdf::Line(10,31,190,31);

                  Fpdf::Cell(30,10,'',0,1);
                  Fpdf::SetFont('Arial', 'B', 14);
                  Fpdf::Cell(185,5,'Transkrip Nilai Sementara',0,0,'C');

                  Fpdf::Cell(30,10,'',0,1);
                  // biodata mahasiswa
                  Fpdf::SetFont('Arial', '', 12);
                  Fpdf::Cell(35,5,'NIM',0,0);
                  Fpdf::Cell(50,5,' : '.$mahasiswa->nim,0,0);
                  Fpdf::Cell(10,5,'',0,0);
                  Fpdf::Cell(35,5,'Jurusan',0,0);
                  Fpdf::Cell(10,5,' : '.$mahasiswa->nama_jurusan,0,1);

                  Fpdf::Cell(35,5,'Nama',0,0);
                  Fpdf::Cell(50,5,' : '.$mahasiswa->nama_mahasiswa,0,0);
                  Fpdf::Cell(10,5,'',0,0);
                  Fpdf::Cell(35,5,'Semester',0,0);
                  Fpdf::Cell(10,5,' : '.$semester_mahasiswa->semester,0,1);

                  Fpdf::Cell(35,5,'Tahun Akademik',0,0);
                  Fpdf::Cell(50,5,' : '.$mahasiswa->tahun_akademik,0,0);
                  Fpdf::Cell(10,5,'',0,0);


                  Fpdf::Cell(30,10,'',0,1);


                  Fpdf::Cell(10,7,'No',1,0);
                  Fpdf::Cell(25,7,'KODE MK',1,0);
                  Fpdf::Cell(105,7,'NAMA MK',1,0);
                  Fpdf::Cell(11,7,'SKS',1,0);
                  Fpdf::Cell(17,7,'GRADE',1,0);
                  Fpdf::Cell(17,7,'BOBOT',1,1);
                  Fpdf::SetFont('Times', '', 11);
                  $no         =1;
                  $totalSKS   =0;
                  $totalMutu  =0;
                  foreach($khs as $row)
                  {
                      $nilai = hitung_nilai($row->id);
                      $grade = hitung_grade($nilai);
                      $mutu  = hitung_mutu($grade);


                      Fpdf::Cell(10,7,$no,1,0);
                      Fpdf::Cell(25,7,$row->kode_mk,1,0);
                      Fpdf::Cell(105,7,$row->nama_mk,1,0);
                      Fpdf::Cell(11,7,$row->jml_sks,1,0);
                      Fpdf::Cell(17,7,$grade,1,0);
                      Fpdf::Cell(17,7,$mutu*$row->jml_sks,1,1);
                      $no++;
                      $totalSKS=$totalSKS+$row->jml_sks;
                      $totalMutu = $totalMutu+$mutu*$row->jml_sks;
                  }

                  Fpdf::Cell(140,7,'Jumlah SKS',1,0);
                  Fpdf::Cell(45,7,$totalSKS,1,1);
                  Fpdf::Cell(140,7,'Mutu',1,0);
                  Fpdf::Cell(45,7,$totalMutu,1,1);
                  Fpdf::Cell(140,7,'IPK',1,0);


                  if(empty($totalMutu) or empty($totalSKS))
                  {
                      $ipk =0;
                  }
                  else
                  {
                      $ipk = $totalMutu/$totalSKS;
                  }

                  Fpdf::Cell(45,7,$ipk,1,1);

                  Fpdf::Cell(30,10,'',0,1);
                  Fpdf::Cell(65,10,'Mahasiswa',0,0);
                  Fpdf::Cell(65,10,'Pembimbing Akademik',0,0);
                  Fpdf::Cell(35,7,'Kaprodi '.$mahasiswa->nama_jurusan,0,1);
                  Fpdf::Cell(100,20,'',0,1);
                  

                  Fpdf::Cell(60,10,$mahasiswa->nama_mahasiswa,0,0);
                  Fpdf::Cell(68,7,$dosbing->nama,0,0);
                  Fpdf::Cell(35,7,$mahasiswa->nama,0,1);

                  Fpdf::Output();
                  exit();
                }


                else{
                  return back()->with('status', 'Data Semester Terpilih Kosong');
                }

    }


    public function merkAjax($id){ //Ajax
      $nim = Auth::guard('mahasiswa')->user()->nim;

        if($id==0){
            $terpilih = \DB::table('khs')
                        ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                        // ->join('dosen','dosen.nidn','=','khs.nidn')
                        ->where('khs.nim',$nim)
                        ->where('khs.semester','1')
                        ->get();
        }else{
        $terpilih =  \DB::table('khs')
                      ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                      // ->join('dosen','dosen.nidn','=','khs.nidn')
                      ->where('khs.nim',$nim)
                      ->where('khs.semester',$id)
                      ->get();
        }


        return $terpilih;
    }


    function KHSpdf()
    {
        $nim = Auth::guard('mahasiswa')->user()->nim;
        $nama = Auth::guard('mahasiswa')->user()->nama_mahasiswa;
        $mahasiswa = \DB::table('mahasiswa')
                    // ->join('jurusan','jurusan.kode_jurusan','=','mahasiswa.kode_jurusan')
                    ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                    ->leftJoin('kelas','mahasiswa.kode_kelas','=','kelas.kode_kelas')
                    ->leftJoin('dosen','kelas.nidn','=','dosen.nidn')
                    ->where('mahasiswa.nim',$nim)
                    ->first();




        $jurusan = \DB::table('mahasiswa')
                    ->leftJoin('jurusan','mahasiswa.kode_jurusan','=','jurusan.kode_jurusan')
                    ->leftJoin('dosen','jurusan.nidn','=','dosen.nidn')
                    ->join('tahun_akademik','tahun_akademik.kode_tahun_akademik','=','mahasiswa.kode_tahun_akademik')
                    ->where('mahasiswa.nim',$nim)
                    ->first();

        $tahunaktif = \DB::table('tahun_akademik')
                  ->where('tahun_akademik.status','y')->first();






        $setting    = \DB::table('setting')->where('id',1)->first();

        $khs = \DB::table('khs')
                ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                ->join('dosen','dosen.nidn','=','khs.nidn')
                ->where('nim',$nim)

                ->get();



        $info = \DB::table('khs')
              ->where('nim',$nim)
              ->orderBy('semester','desc')
              ->first();




        Fpdf::AddPage();
        Fpdf::SetFont('Arial', 'B', 18);
        Fpdf::Image('https://4.bp.blogspot.com/-nOCPZVY8wdU/XJeti2m55eI/AAAAAAAAAQU/ih5ivB83uLM1IqXEX5OtzjBQdZ-rp_N5QCLcBGAs/s1600/aasasa.png',10,5,-1000);
        Fpdf::Cell(185,7,$setting->nama_universitas,0,1,'C');

        Fpdf::SetFont('Arial', '', 12);
        Fpdf::Cell(185,5,$setting->alamat,0,1,'C');
        Fpdf::SetFont('Arial', '', 12);
        Fpdf::Cell(185,5,"Telpon : ".$setting->no_telpon." | Email : ".$setting->email.' | Fax : '.$setting->fax,0,1,'C');

        Fpdf::Line(10,30,190,30);
        Fpdf::Line(10,31,190,31);

        Fpdf::Cell(30,10,'',0,1);
        Fpdf::SetFont('Arial', 'B', 14);
        Fpdf::Cell(185,5,'Transkrip Nilai Sementara',0,0,'C');

        Fpdf::Cell(30,10,'',0,1);
        // biodata mahasiswa
        Fpdf::SetFont('Arial', '', 12);
        Fpdf::Cell(35,5,'NIM',0,0);
        Fpdf::Cell(50,5,' : '.$mahasiswa->nim,0,0);
        Fpdf::Cell(10,5,'',0,0);
        Fpdf::Cell(35,5,'Jurusan',0,0);
        Fpdf::Cell(10,5,' : '.$jurusan->nama_jurusan,0,1);

        Fpdf::Cell(35,5,'Nama',0,0);
        Fpdf::Cell(50,5,' : '.$mahasiswa->nama_mahasiswa,0,0);
        Fpdf::Cell(10,5,'',0,0);
        if(!empty($info)){
        Fpdf::Cell(35,5,'Semester Tempuh',0,0);
        Fpdf::Cell(10,5,' : '.$info->semester,0,1);
      }
      if(empty($info)){
      Fpdf::Cell(35,5,'Semester Tempuh',0,0);
      Fpdf::Cell(10,5,' : '.'1',0,1);
    }

        Fpdf::Cell(35,5,'Tahun Akademik',0,0);
        Fpdf::Cell(50,5,' : '.$tahunaktif->tahun_akademik,0,0);
        Fpdf::Cell(10,5,'',0,0);


        Fpdf::Cell(30,10,'',0,1);


        Fpdf::Cell(10,7,'No',1,0);
        Fpdf::Cell(25,7,'KODE MK',1,0);
        Fpdf::Cell(105,7,'NAMA MK',1,0);
        Fpdf::Cell(11,7,'SKS',1,0);
        Fpdf::Cell(17,7,'GRADE',1,0);
        Fpdf::Cell(17,7,'BOBOT',1,1);
        Fpdf::SetFont('Times', '', 11);
        $no         =1;
        $totalSKS   =0;
        $totalMutu  =0;
        foreach($khs as $row)
        {
            $nilai = hitung_nilai($row->id);
            $grade = hitung_grade($nilai);
            $mutu  = hitung_mutu($grade);


            Fpdf::Cell(10,7,$no,1,0);
            Fpdf::Cell(25,7,$row->kode_mk,1,0);
            Fpdf::Cell(105,7,$row->nama_mk,1,0);
            Fpdf::Cell(11,7,$row->jml_sks,1,0);
            Fpdf::Cell(17,7,$grade,1,0);
            Fpdf::Cell(17,7,$mutu*$row->jml_sks,1,1);
            $no++;
            $totalSKS=$totalSKS+$row->jml_sks;
            $totalMutu = $totalMutu+$mutu*$row->jml_sks;
        }

        Fpdf::Cell(140,7,'Jumlah SKS',1,0);
        Fpdf::Cell(45,7,$totalSKS,1,1);
        Fpdf::Cell(140,7,'Mutu',1,0);
        Fpdf::Cell(45,7,$totalMutu,1,1);
        Fpdf::Cell(140,7,'IPK',1,0);


        if(empty($totalMutu) or empty($totalSKS))
        {
            $ipk =0;
        }
        else
        {
            $ipk = $totalMutu/$totalSKS;
        }

        Fpdf::Cell(45,7,$ipk,1,1);

        Fpdf::Cell(30,10,'',0,1);
        Fpdf::Cell(65,10,'Mahasiswa',0,0);
        Fpdf::Cell(65,10,'Pembimbing Akademik',0,0);
        Fpdf::Cell(35,7,'Kaprodi '.$jurusan->nama_jurusan,0,1);
        Fpdf::Cell(100,20,'',0,1);
        // Fpdf::Cell(1,10,'',0,0);

        Fpdf::Cell(65,10,$mahasiswa->nama_mahasiswa,0,0);
        Fpdf::Cell(60,10,$mahasiswa->nama,0,0);
        Fpdf::Cell(35,7,$jurusan->nama,0,1);

        Fpdf::Output('I',"Transkrip_$nama");
        exit();

    }

    function KHSShow(){
      $nim = Auth::guard('mahasiswa')->user()->nim;

      $khs = \DB::table('khs')
              ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
              // ->join('dosen','dosen.nidn','=','khs.nidn')
              ->where('nim',$nim)
              ->get();

              return view('khs.rincian_nilai')->with('nilai_list',$khs);
    }

     function getStateList(Request $request)
          {
              $states = \DB::table("khs")
                        ->leftJoin('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                        ->where("semester",$request->semester)
                        ->pluck("matakuliah.nama_mk","matakuliah.kode_mk");
              return response()->json($states);
          }


}
