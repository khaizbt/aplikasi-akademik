<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Dosen;
use Auth;
use Fpdf;
use DB;
use DateTime;

use App\Exports\DosenExport;
use Maatwebsite\Excel\Facades\Excel;
class DosenController extends Controller
{
    function json(){
        $dosen = \DB::table('dosen')
                ->get();

        return Datatables::of($dosen)
        ->addColumn('action', function ($dosen) {
          '<a href="/matakuliah/'.$dosen->nidn.'/edit" class="btn btn-primary btn-sm">EdIT</a>';
              return view('modal.dosen' , [
             'model' => $dosen,
               'url_show' => route('dosen.show',$dosen->nidn),
               'url_edit' => route('dosen.edit', $dosen->nidn),
               'url_destroy' => route('dosen.destroy', $dosen->nidn)
          ]);
      })
      ->rawColumns(['action'])
      ->make(true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('dosen.index_update');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nidn' => 'required|unique:dosen|min:4',
            'nama' => 'required|min:6',
            'tempat_lahir' => 'required|min:5',
            'tanggal_lahir' => 'required|min:6',
            'email'     =>'required|unique:dosen',
            'alamat'     =>'required',
            'no_hp'     =>'required'
        ]);


        $dosen = New dosen();
        $dosen->nidn        = $request->nidn;

        $dosen->alamat      = $request->alamat;
        $dosen->tempat_lahir      = $request->tempat_lahir;
        $dosen->tanggal_lahir   = $request->tanggal_lahir;
        $dosen->jenis_kelamin   = $request->jenis_kelamin;

        $dosen->nama        = $request->nama;
        $dosen->no_hp       = $request->no_hp;
        $dosen->email       = $request->email;

        $dosen->password    = $request->password;
        $dosen->save();
        return redirect('/dosen')->with('status','Data dosen Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nidn)
    {
        $user = \App\Dosen::findOrFail($nidn);

        $tanggal = DateTime::createFromFormat('Y-m-d', $user->tanggal_lahir);


        return view('dosen.show', ['dosen' => $user], ['tanggal_lahir'=> $tanggal->format('d-M-Y')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($nidn)
    {

        $data['dosen'] = dosen::where('nidn',$nidn)->first();
        return view('dosen.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nidn)
    {
      $request->validate([
          'nama' => 'required|min:4',
          'tempat_lahir' => 'required|min:5',
          'tanggal_lahir' => 'required|min:6',
          'email'     =>'required',
          'alamat'     =>'required',
          'no_hp'     =>'required'
      ]);


        $dosen = dosen::where('nidn',$nidn)->first();
        $dosen->nidn              = $request->nidn;

        $dosen->nama              = $request->nama;
        $dosen->alamat            = $request->alamat;
        $dosen->tempat_lahir      = $request->tempat_lahir;
        $dosen->tanggal_lahir     = $request->tanggal_lahir;
        $dosen->no_hp             = $request->no_hp;

        $dosen->email             = $request->email;
        if($request->password!='')
        {
            $dosen->password    = $request->password;
        }
        $dosen->update();
        return redirect('/dosen')->with('status','Data dosen Berhasil Di Update');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nidn)
    {
        $dosen = dosen::where('nidn',$nidn);
        $dosen->delete();
        return redirect('/dosen')->with('status','Data dosen Berhasil Dihapus');;
    }


    function jadwal_mengajar(){
      $tahun_akademik = \DB::table('tahun_akademik')
                ->where('tahun_akademik.status','y')->first();

                $awal_kuliah = DateTime::createFromFormat('Y-m-d', $tahun_akademik->tanggal_awal_kuliah);
                $akhir_kuliah = DateTime::createFromFormat('Y-m-d', $tahun_akademik->tanggal_akhir_kuliah);



                  return view('dosen.jadwal_mengajar', ['tahun' => $tahun_akademik],['tanggal_awal_kuliah'=> $awal_kuliah->format('d-M-Y'),'tanggal_akhir_kuliah'=> $akhir_kuliah->format('d-M-Y')]);
        // return view('dosen.jadwal_mengajar');
    }

    function jadwal_mengajar_json(){
      $result = '<table class="table table-bordered">
              <tr><th width="300">Nama Matakuliah</th><th width="90">hari</th><th width="150">Teori(Jam/Ruang)</th><th width="130">Praktikum(Jam/Ruang)</th><th width="130">Kelas</th><th width="100">Action</th><tr>';

              $tahunaktif = \DB::table('tahun_akademik')
                        ->where('tahun_akademik.status','y')->first();

                $jadwal = \DB::table('jadwal_kuliah')
                        ->leftJoin('ruangan','ruangan.kode_ruangan','=','jadwal_kuliah.kode_ruangan')
                        ->leftjoin('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                        ->leftJoin('jam_kuliah','jam_kuliah.id','=','jadwal_kuliah.jam')
                        ->leftJoin('jurusan','jurusan.kode_jurusan','=','jadwal_kuliah.kode_jurusan')
                        ->leftJoin('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                        ->join('tahun_akademik','jadwal_kuliah.kode_tahun_akademik','=','tahun_akademik.kode_tahun_akademik')
                        ->where('tahun_akademik.kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
                        ->where('jadwal_kuliah.nidn',Auth::guard('dosen')->user()->nidn)
                        ->select('jadwal_kuliah.id','matakuliah.nama_mk','jadwal_kuliah.hari','jam_kuliah.jam','ruangan.nama_ruangan','jurusan.nama_jurusan','kelas.nama_kelas')
                        ->get();

                             foreach($jadwal as $word)
                             {

                               $praktikum = DB::table('jadwal_kuliah')
                                           ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                                           ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                                           ->where('jadwal_kuliah.id',$word->id)
                                           ->get()->toArray();

                                           if(!empty($praktikum)){

                                           $result .= '<tr>
                                                       <td>'.$word->nama_mk.'</td>

                                                       <td>'.$word->hari.'</td>
                                                       <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                       <td>'.$praktikum[0]->jam.'/'.$praktikum[0]->nama_ruangan.'</td>
                                                       <td>'.$word->nama_kelas.'</td>
                                                       <td><a href="/nilai/'.$word->id.'" class="btn btn-primary btn-sm"><i class="fas fa-address-card"></i> Nilai</a>
                                                       </tr>';
                                                     }
                                                     else{
                                                       $result .= '<tr>
                                                                   <td>'.$word->nama_mk.'</td>

                                                                   <td>'.$word->hari.'</td>
                                                                   <td>'.$word->jam.'/'.$word->nama_ruangan.'</td>
                                                                   <td>'.'-'.'</td>
                                                                   <td>'.$word->nama_kelas.'</td>
                                                                   <td><a href="/nilai/'.$word->id.'" class="btn btn-primary btn-sm"><i class="fas fa-address-card"></i> Nilai</a>
                                                                   </tr>';

                                                     }
                             }


             $result .='</table>';


            return $result;


      // $tahunaktif = \DB::table('tahun_akademik')
      //           ->where('tahun_akademik.status','y')->first();
      //
      //   $jadwal = \DB::table('jadwal_kuliah')
      //           ->leftJoin('ruangan','ruangan.kode_ruangan','=','jadwal_kuliah.kode_ruangan')
      //           ->leftjoin('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
      //           ->leftJoin('jam_kuliah','jam_kuliah.id','=','jadwal_kuliah.jam')
      //           ->leftJoin('jurusan','jurusan.kode_jurusan','=','jadwal_kuliah.kode_jurusan')
      //           ->leftJoin('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
      //           ->join('tahun_akademik','jadwal_kuliah.kode_tahun_akademik','=','tahun_akademik.kode_tahun_akademik')
      //           ->where('tahun_akademik.kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
      //           ->where('jadwal_kuliah.nidn',Auth::guard('dosen')->user()->nidn)
      //           ->select('jadwal_kuliah.id','matakuliah.nama_mk','jadwal_kuliah.hari','jam_kuliah.jam','ruangan.nama_ruangan','jurusan.nama_jurusan','kelas.nama_kelas')
      //           ->get();
      //
      //
      //
      //
      //
      //
      //
      //   return Datatables::of($jadwal)
      //   ->addColumn('action', function ($row) {
      //       $action  = '<a href="/nilai/'.$row->id.'" class="btn btn-primary btn-sm"><i class="fas fa-address-card"></i> Nilai</a> ';
      //       //$action .= \Form::open(['url'=>'dosen/'.$row->nidn,'method'=>'delete','style'=>'float:right']);
      //       //$action .= "<button type='submit'class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";
      //       return $action;
      //   })
      //   ->make(true);

    }


    function exportExcel()
    {
        return Excel::download(new DosenExport, 'data-dosen.xlsx');
    }

    public function ajaxSearch(Request $request){
        $keyword = $request->get('q');

        $datadosen = \App\Dosen::where("nama","LIKE","%$keyword%")
        ->get();

        return $datadosen;
    }

    function DownloadJadwal(){
      $nidn = Auth::guard('dosen')->user()->nidn;
      $nama = Auth::guard('dosen')->user()->nama;

      $dosen = \DB::table('dosen')
                  ->where('dosen.nidn',$nidn)
                  ->first();



      $tahunaktif = \DB::table('tahun_akademik')
                ->where('tahun_akademik.status','y')->first();


      $setting    = \DB::table('setting')->where('id',1)->first();

      $jadwalmengajar = \DB::table('jadwal_kuliah')
                      ->join('dosen','jadwal_kuliah.nidn','=','dosen.nidn')
                      ->join('khs','jadwal_kuliah.id','=','khs.id_jadwal')
                      ->join('matakuliah','jadwal_kuliah.kode_mk','=','matakuliah.kode_mk')
                      ->join('ruangan','jadwal_kuliah.kode_ruangan','=','ruangan.kode_ruangan')
                      ->join('kelas','jadwal_kuliah.kode_kelas','=','kelas.kode_kelas')
                      ->join('jam_kuliah','jadwal_kuliah.jam','=','jam_kuliah.id')
                      ->where('khs.nidn',$nidn)
                      ->select('jadwal_kuliah.id','jadwal_kuliah.hari','jam_kuliah.jam','matakuliah.nama_mk','dosen.nama','kelas.nama_kelas','ruangan.nama_ruangan')
                      ->get();








      Fpdf::AddPage('L');
      Fpdf::SetFont('Arial', 'B', 18);
      Fpdf::Image('https://4.bp.blogspot.com/-nOCPZVY8wdU/XJeti2m55eI/AAAAAAAAAQU/ih5ivB83uLM1IqXEX5OtzjBQdZ-rp_N5QCLcBGAs/s1600/aasasa.png',10,5,-1000);
      Fpdf::Cell(185,7,                 $setting->nama_universitas,0,1,'C');

      Fpdf::SetFont('Arial', '', 12);
      Fpdf::Cell(185,5,$setting->alamat,0,1,'C');
      Fpdf::SetFont('Arial', '', 12);
      Fpdf::Cell(185,5,"Telpon : ".$setting->no_telpon." | Email : ".$setting->email.' | Fax : '.$setting->fax,0,1,'C');

      Fpdf::Line(10,30,190,30);
      Fpdf::Line(10,31,190,31);

      Fpdf::Cell(30,10,'',0,1);
      Fpdf::SetFont('Arial', 'B', 14);
      Fpdf::Cell(185,5,'Jadwal Mengajar',0,0,'C');

      Fpdf::Cell(30,10,'',0,1);
      // biodata mahasiswa
      Fpdf::SetFont('Arial', '', 12);
      Fpdf::Cell(35,5,'NIM',0,0);
      Fpdf::Cell(50,5,' : '.$dosen->nidn,0,1);


      Fpdf::Cell(35,5,'Nama',0,0);
      Fpdf::Cell(50,5,' : '.$dosen->nama,0,1);


      Fpdf::Cell(35,5,'Tahun Akademik',0,0);
      Fpdf::Cell(50,5,' : '.$tahunaktif->tahun_akademik,0,0);
      Fpdf::Cell(10,5,'',0,0);


      Fpdf::Cell(30,10,'',0,1);


      Fpdf::Cell(10,7,'No',1,0);
      Fpdf::Cell(20,7,'HARI',1,0);
      Fpdf::Cell(25,7,'TEORI',1,0);
      Fpdf::Cell(25,7,'PRAKTIKUM',1,0);
      Fpdf::Cell(80,7,'NAMA MK',1,0);
      Fpdf::Cell(30,7,'KELAS',1,0);
      Fpdf::Cell(35,7,'Ruang Teori',1,0);
      Fpdf::Cell(40,7,'Ruang Praktikum',1,1);

      Fpdf::SetFont('Times', '', 11);
      $no         =1;

      foreach($jadwalmengajar as $row)
      {
        $praktikum = DB::table('jadwal_kuliah')
                    ->join('jam_kuliah','jadwal_kuliah.jam_praktik','=','jam_kuliah.id')
                    ->join('ruangan','jadwal_kuliah.ruang_praktik','=','ruangan.kode_ruangan')
                    ->where('jadwal_kuliah.id',$row->id)
                    ->get()->toArray();



          if(empty($praktikum)){


          Fpdf::Cell(10,7,$no++,1,0);
          Fpdf::Cell(20,7,$row->hari,1,0);
          Fpdf::Cell(25,7,$row->jam,1,0);
          Fpdf::Cell(25,7,'-',1,0);
          Fpdf::Cell(80,7,$row->nama_mk,1,0);
          Fpdf::Cell(30,7,$row->nama_kelas,1,0);
          Fpdf::Cell(35,7,$row->nama_ruangan,1,0);
          Fpdf::Cell(40,7,'-',1,1);
        }
        else{
          Fpdf::Cell(10,7,$no++,1,0);
          Fpdf::Cell(20,7,$row->hari,1,0);
          Fpdf::Cell(25,7,$row->jam,1,0);
          Fpdf::Cell(25,7,$praktikum[0]->jam,1,0);
          Fpdf::Cell(80,7,$row->nama_mk,1,0);
          Fpdf::Cell(30,7,$row->nama_kelas,1,0);
          Fpdf::Cell(35,7,$row->nama_ruangan,1,0);
          Fpdf::Cell(40,7,$praktikum[0]->ruang_praktik,1,1);
        }


      }
      Fpdf::Output('I',"Jadwal Mengajar_$nidn");
      exit();
    }
}
