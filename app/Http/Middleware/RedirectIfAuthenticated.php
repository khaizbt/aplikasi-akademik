<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      if (Auth::guard('mahasiswa')->check()) {
              return redirect ('/mahasiswa/home');
            }
            if (Auth::guard('dosen')->check()) {
                    return redirect ('/jadwal_mengajar');
                  }

                        if (Auth::guard($guard)->check()) {
                            return redirect('/home');
                        }


                        


        return $next($request);
    }
}
