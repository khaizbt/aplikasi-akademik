<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Dosen extends Authenticatable
{
    protected $guard = 'dosen';
    protected $table="dosen";

    protected $fillable=['nidn','nama','email','jenis_kelamin','no_hp','password','alamat','tempat_lahir','tanggal_lahir'];

    protected $primaryKey = "nidn";

    public $incrementing = false;

    function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }



}
