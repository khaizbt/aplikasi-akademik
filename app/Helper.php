<?php

function get_tahun_akademik($field){

    $tahun_akademik = \DB::table('tahun_akademik')
                        ->where('status','y')
                        ->first();
    return $tahun_akademik->$field;
}


function chek_kehadiran($nim,$id_jadwal,$pertemuan_ke)
{
    $jadwal_kuliah = DB::table('jadwal_kuliah')
                    ->where('id',$id_jadwal)->first();

    $kehadiran = DB::table('kehadiran')
                ->where('kode_mk',$jadwal_kuliah->kode_mk)
                ->where('nidn',$jadwal_kuliah->nidn)
                ->where('kode_jurusan',$jadwal_kuliah->kode_jurusan)
                ->where('kode_ruang',$jadwal_kuliah->kode_ruangan)
                ->where('kode_tahun_akademik',$jadwal_kuliah->kode_tahun_akademik)
                ->orderBy('updated_at', 'desc')
                ->first();




    $riwayat  = DB::table('riwayat_kehadiran')
                ->where('nim',$nim)
                // ->where('riwayat_kehadiran.kehadiran_id',$kehadiran->id)
                ->where('pertemuan_ke',$pertemuan_ke);




    if($riwayat->count()>0)
    {
        $riwayat = $riwayat->first();
        return $riwayat->status_kehadiran;
    }else{
        return '-';
    }

}




// Sumber : https://www.youthmanual.com/post/dunia-kuliah/kehidupan-mahasiswa/bagaimana-cara-menghitung-ipk-ini-dia-rumusnya

function hitung_nilai($idKHS)
{

    $khs        = DB::table('khs')
                    ->where('id',$idKHS)
                    ->join('matakuliah','matakuliah.kode_mk','=','khs.kode_mk')
                    ->first();

    $kontrak    = \DB::table('kontrak')
                ->where('nidn',$khs->nidn)
                ->where('kode_mk',$khs->kode_mk)
                ->where('kode_kelas',$khs->kode_kelas)
                ->select('kontrak.kontrak_uts','kontrak.kontrak_uas','kontrak.kontrak_tugas','kontrak.kontrak_kehadiran')
                ->first();


    $uts        = $khs->nilai_uts*($kontrak->kontrak_uts/100);
    $uas        = $khs->nilai_uas*($kontrak->kontrak_uas/100);
    $tugas      = $khs->nilai_tugas*($kontrak->kontrak_tugas/100);
    $kehadiran  = $khs->nilai_kehadiran*($kontrak->kontrak_kehadiran/100);
    $hasil      = $uts+$uas+$tugas+$kehadiran;

    // $jumlahbobot = $grade*'jml_sks';
    return $hasil;

}





function hitung_mutu($grade)
{
    if($grade=='A')
    {
        $mutu = 4;
    }elseif($grade=='A-')
    {
        $mutu=3.70;
    }
    elseif($grade=='B+')
    {
        $mutu=3.30;
    }
    elseif($grade=='B')
    {
        $mutu=3;
    }
    elseif($grade=='B-')
    {
        $mutu=2.70;
    }
    elseif($grade=='C+')
    {
        $mutu=2.30;
    }
    elseif($grade=='C')
    {
        $mutu = 2;
    }
    elseif($grade=='C-')
    {
        $mutu=1.70;
    }
    elseif($grade=='D')
    {
        $mutu=1;
    }else{
        $mutu=0;
    }

    return $mutu;
}

function hitung_grade($nilai)
{
    if($nilai>85)
    {
        $grade = "A";
    }
    elseif($nilai>80)
    {
        $grade="A-";
    }
    elseif($nilai>75)
    {
        $grade="B+";
    }
    elseif($nilai>70)
    {
        $grade="B";
    }
    elseif($nilai>65)
    {
        $grade="B-";
    }
    elseif($nilai>60)
    {
        $grade="C+";
    }
    elseif($nilai>55)
    {
        $grade ="C";
    }
    elseif($nilai>50)
    {
        $grade="C-";
    }
    elseif($nilai>40)
    {
        $grade = "D";
    }else
    {
        $grade = "E";
    }

    // $m = $grade * $khs->jml_sks;

    return $grade;
}



function kirim_sms($tujuan,$pesan)
{
    $apiKey ="";
    $apiPass="";
}


function kirim_whatapps($tujuan,$pesan)
{
    $apiKey ="";
    $apiPass="";
}
