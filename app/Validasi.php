<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Validasi extends Model
{
  protected $table="validasi";

  protected $fillable=['nim','kode_kelas','kode_tahun_akademik','status','semester'];
}
