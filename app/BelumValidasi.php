<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BelumValidasi extends Model
{
  protected $table="belumvalidasi";

  protected $fillable=['nim','kode_kelas','kode_tahun_akademik'];

}
