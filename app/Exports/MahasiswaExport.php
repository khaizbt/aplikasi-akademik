<?php

namespace App\Exports;

use App\Mahasiswa;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MahasiswaExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $cek = \DB::table('belum_validasi')
      // ->join('mahasiswa','khs.nim','=','mahasiswa.nim')
      // ->where('id_jadwal',$this->id_jadwal)
      // ->select('khs.nim','nama_mahasiswa','khs.nilai_kehadiran','khs.nilai_tugas','khs.nilai_uts','khs.nilai_uas','nidn','khs.kode_mk','khs.kode_tahun_akademik')
      ->get();

      // dd($cek);




        return $cek;
    }
    protected $headings;

    public function __construct($headings) {

    $this->headings = $headings;
    }
    public function headings() : array
    {
        return $this->headings;
    }
}
