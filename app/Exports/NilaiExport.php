<?php

namespace App\Exports;

use App\Khs;
use App\Jadwalkuliah;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
class NilaiExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $id;

    public function __construct($id,$headings) {
    $this->id_jadwal = $id;
    $this->headings = $headings;
    }

    public function collection()
    {

      $cek = \DB::table('khs')
      ->join('mahasiswa','khs.nim','=','mahasiswa.nim')
      ->where('id_jadwal',$this->id_jadwal)
      ->select('khs.nim','nama_mahasiswa','khs.nilai_kehadiran','khs.nilai_tugas','khs.nilai_uts','khs.nilai_uas','nidn','khs.kode_mk','khs.kode_tahun_akademik')
      ->get();

      // dd($cek);




        return $cek;



        // Khs::findOrFail($this->id_jadwal);
    }
    public function headings() : array
    {
        return $this->headings;
    }
}
