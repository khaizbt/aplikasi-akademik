<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
  protected $table="pengajuan";

  protected $fillable=['kode_mk','nim','semester','jml_sks'];
}
