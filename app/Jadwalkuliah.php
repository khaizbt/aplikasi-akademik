<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwalkuliah extends Model
{
    protected $table="jadwal_kuliah";

    protected $fillable=['id','hari','kode_mk','kode_kelas','nidn','kode_ruangan','jam','semester','kode_tahun_akademik','kode_jurusan','ruang_praktik','jam_praktik'];
}
