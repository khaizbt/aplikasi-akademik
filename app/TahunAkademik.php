<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TahunAkademik extends Model
{
    protected $table="tahun_akademik";
    protected $fillable=['kode_tahun_akademik','tahun_akademik','status','tanggal_awal_kuliah',
                        'tanggal_akhir_kuliah','tanggal_awal_krs','tanggal_akhir_krs',
                        'tanggal_awal_pengajuan','tanggal_akhir_pengajuan'];
    protected $primaryKey = 'kode_tahun_akademik';

}
