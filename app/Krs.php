<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Krs extends Model
{
    protected $table="krs";

    protected $fillable=['nim','kode_mk','nidn'];
}
