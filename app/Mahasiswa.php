<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mahasiswa extends Authenticatable
{
    protected $guard = 'mahasiswa';


    protected $table="mahasiswa";

    protected $fillable=['nim','nama_mahasiswa','nama_ayah','nama_ibu','jenis_kelamin','email','no_hp','password','alamat','kode_jurusan','kode_tahun_akademik','semester_aktif','kode_kelas','tempat_lahir','tanggal_lahir'];

    protected $primaryKey = 'nim';

    public $incrementing = false;



    function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
