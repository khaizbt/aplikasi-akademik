<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
    protected $table="kontrak";

    protected $fillable=['id_jadwal','kode_mk','nidn','kontrak_kehadiran','kontrak_uas','kontrak_uts','kontrak_tugas','semester'];
}
