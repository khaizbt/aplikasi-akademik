<?php

namespace App\Imports;

use App\Mahasiswa;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;



class MahasiswaImport implements ToModel, WithHeadingRow, WithChunkReading, ShouldQueue, WithValidation

{
  use Importable;


    public function model(array $row)
    {
      $tahunaktif = \DB::table('tahun_akademik')
                    ->where('status','y')
                    ->first();

      $data = Mahasiswa::find($row['nim']);
       if (empty($data)) {

          return new Mahasiswa([
          'nim' => $row['nim'],
          'slug' => str_slug($row['nim']),
          'nama_mahasiswa' => $row['nama_mahasiswa'],
          'email' => 'example@gmail.com',
          'kode_kelas' => $row['kode_kelas'],
          'alamat' => $row['alamat'],
          'kode_jurusan' => $row['kode_jurusan'],
          'kode_tahun_akademik' => $tahunaktif->kode_tahun_akademik,
          'no_hp' => '62800000000',
          'tempat_lahir' => $row['tempat_lahir'],
          'tanggal_lahir' => $row['tanggal_lahir'],
          'password' => '123456'
        ]);
    }
  }

  public function rules(): array
    {
        return [
        'nim' => 'required',
        'nama_mahasiswa' => 'required',
        'kode_kelas' => 'required',
        'alamat' => 'required',
        'kode_jurusan' => 'required',
        'tanggal_lahir' => 'required',
        'tempat_lahir' => 'required',
        ];
        }

        public function customValidationMessages()
{
    return [
        'nim.required' => 'NIM Harus Diisi.',
        'nama_mahasiswa.required' => 'Nama Mahasiswa Harus Diisi.',
        'email.required' => 'Email Harus Diisi.',
        'kode_kelas.required' => 'Email Harus Diisi.',
        'alamat.required' => 'Email Harus Diisi.',
        'kode_jurusan.required' => 'Email Harus Diisi.',

    ];
}



    public function chunkSize(): int
    {
        return 1000;
    }
}
