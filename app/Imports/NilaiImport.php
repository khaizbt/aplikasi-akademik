<?php

namespace App\Imports;

use App\Khs;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;

class NilaiImport implements ToModel, WithHeadingRow, WithValidation
{

  use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Khs([
          'id_jadwal' => $row['id_jadwal'],
          'slug' => str_slug($row['id_jadwal']),
          'nim' => $row['nim'],
          'nama_mahasiswa' => $row['nama_mahasiswa'],
          'kode_mk' => $row['kode_mk'],
          'nilai_uts' => $row['nilai_uts'],
          'nilai_uas' => $row['nilai_uas'],
          'nilai_tugas' => $row['nilai_tugas'],
          'nilai_kehadiran' => $row['nilai_kehadiran'],
          'nidn' => $row['nidn'],
          'semester' => $row['semester'],
          'kode_kelas' => $row['kode_kelas'],
        ]);

    }

    public function rules(): array
      {
          return [
          'id_jadwal' => 'required',
          'nim' => 'required',
          'kode_mk' => 'required',
          'nidn' => 'required',
          'semester' => 'required',
          'kode_kelas' => 'required'
          ];
          }

          public function customValidationMessages()
  {
      return [
          'id_jadwal.required' => 'ID Jadwal Harus Diisi.',
          'nim.required' => 'NIM Harus Diisi.',
          'kode_mk.required' => 'Kode MK Harus Diisi.',
          'kode_kelas.required' => 'Email Harus Diisi.',
                ];
  }

}
