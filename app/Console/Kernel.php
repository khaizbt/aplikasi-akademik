<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\TahunAkademik;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      $schedule->call(function () {

          $tahunaktif = \DB::table('tahun_akademik')
                        ->where('tahun_akademik.status','y')->first();
          $date1 = strtotime("today");
          $date2 = strtotime($tahunaktif->tanggal_akhir_kuliah);

          if ($date1 > $date2){
          $id = \DB::table('khs')
                    ->where('kode_tahun_akademik',$tahunaktif->kode_tahun_akademik)
                    ->groupBy('id_jadwal')
                    ->select('id_jadwal')
                    ->get();

          // $tanggal = \DB::table('tahun_akademik')
          //           ->where('status','y')
          //           ->update([
          //           'status'         =>  'n'
          //           ]);


          foreach($id as $row) {
            $hitungkehadiran = \DB::table('khs')
                      ->where('id_jadwal',$row->id_jadwal)
                      ->where('nilai_kehadiran',0)
                      ->select('id_jadwal')
                      ->count();

                      $hitungsemua = \DB::table('khs')
                                          ->where('id_jadwal',$row->id_jadwal)
                                          ->select('id_jadwal')
                                          ->count();


                        if ($hitungkehadiran == $hitungsemua) {


                                  $nilai = \DB::table('khs')
                                          ->where('id_jadwal',$row->id_jadwal)
                                          ->update([
                                              'nilai_kehadiran'   =>  75,
                                              'nilai_tugas'       =>  75,
                                              'nilai_uts'         =>  75,
                                              'nilai_uas'         =>  75

                                              ]);
                                  $kontrak = \DB::table('kontrak')
                                          ->where('id_jadwal',$row->id_jadwal)
                                          ->update([
                                              'kontrak_kehadiran'   =>  10,
                                              'kontrak_tugas'       =>  20,
                                              'kontrak_uts'         =>  30,
                                              'kontrak_uas'         =>  40
                                              ]);

                        }

                      }
                    }
      })->daily();

      $schedule->call(function () {
        $tahunaktif = \DB::table('tahun_akademik')
                      ->where('status','y')->first();

        if(empty($tahunaktif)){
        $max = TahunAkademik::where('status', 'n')->max('kode_tahun_akademik');

            $tahun_akademik = \DB::table('tahun_akademik')
                    ->where('kode_tahun_akademik',$max)
                    ->update([
                        'status'   =>  'y',
                        ]);

      }
    })->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
