<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKontraksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontrak', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nidn');
            $table->string('kode_mk');
            $table->string('kode_tahun_akademik');
            $table->string('semester');
            $table->integer('kontrak_kehadiran');
            $table->integer('kontrak_tugas');
            $table->integer('kontrak_uts');
            $table->integer('kontrak_uas');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontraks');
    }
}
