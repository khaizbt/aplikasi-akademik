<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Carbon\Carbon;


Route::get('/', function () {
    return view('welcome');
});
Route::get('/','WelcomeController@index');
Route::get('/cek','NilaiController@penilaian');

Route::get('/mahasiswa/login','LoginMahasiswaController@index');
Route::post('/mahasiswa/login','LoginMahasiswaController@submit');
Route::get('/dosen/login','LoginDosenController@index');
Route::post('/dosen/login','LoginDosenController@submit');

// Guard Dosen
Route::group(['middleware' => ['auth:dosen']], function () {
    Route::get('dosen/home', 'HomeController@index_dosen');
    Route::get('jadwal_mengajar','DosenController@jadwal_mengajar');
    Route::get('jadwal_mengajar/json','DosenController@jadwal_mengajar_json');
    Route::get('jadwal_mengajar/download','DosenController@DownloadJadwal');
    Route::post('/kehadiran/update','KehadiranController@update');
    Route::post('kehadiran/{id_jadwal}','KehadiranController@store');
    Route::get('/kehadiran/{id_kehadiran}/absen','KehadiranController@show');
    Route::get('/settingdosen','SettingDosenController@index');
    Route::put('/settingdosen','SettingDosenController@update');
    Route::get('/nilai/{id}/import', 'NilaiController@importIndex');
    Route::post('/nilai/{id}/import', 'NilaiController@Import');
    Route::get('nilai/{id}/download','NilaiController@download');



    Route::get('nilai/{id}','NilaiController@index');

    Route::get('kehadiran/{id_jadwal}/create','KehadiranController@create');
    Route::get('kehadiran/{id_jadwal}','KehadiranController@index');
    Route::post('/nilai/update_nilai/update','NilaiController@update_nilai');
    Route::post('/kontrak/update_kontrak/update','NilaiController@update_kontrak');
});


// Guard Mahasiswa
Route::group(['middleware' => ['auth:mahasiswa']], function () {
    Route::get('mahasiswa/home', 'HomeController@index_mahasiswa');
    Route::get('/krs/daftarMatakuliahKontrak','KrsController@daftarMatakuliahKontrak');
    Route::get('/krs/kelas','KrsController@kelas');
    Route::get('/krs/daftarPengajuan','KrsController@daftarPengajuan');
    Route::get('/krs','KrsController@index');
    Route::get('/krs/pengajuan','KrsController@indexPengajuan');
    Route::get('/krs/tambahPengajuan','KrsController@tambahPengajuan');
    Route::get('/krs/tampilPengajuan','KrsController@tampilPengajuan');
    Route::get('/krs/tampilJadwal','KrsController@DaftarMatakuliahKontrak');
    Route::post('/krs/hapusPengajuan','KrsController@hapusPengajuan');
    Route::get('/krs/json','KrsController@cetak');
    Route::get('/krs/cetak','KrsController@IndexCetak');
    Route::get('/krs/pdf','KrsController@CetakPdf');
    Route::get('/jadwal_kuliah/pdf','JadwalMahasiswaController@DownloadJadwal');
    Route::get('/khs/khs_semester', 'KhsController@KHSSemester');
    Route::get('/krs/tambahKrs','KrsController@tambahKrs');
    Route::get('/krs/tampilKrs','KrsController@tampilKrs');
    Route::post('/krs/hapusKrs','KrsController@hapusKrs');
    Route::get('/khs/transkrip_nilai','KhsController@index');
    Route::get('/khs/pdf','KhsController@KHSpdf');
    Route::get('/khs/rincian_nilai','KHSController@KHSShow');
    Route::get('get-state-list','KHSController@getStateList');
    Route::get('/khs/download','KhsController@SemesterPdf');
    Route::get('/khs/khs_semester/{id}','KHSController@merkAjax');
    Route::get('/settingmahasiswa','SettingMahasiswaController@index');
    Route::put('/settingmahasiswa','SettingMahasiswaController@update');
    Route::get('/jadwal_kuliah','JadwalMahasiswaController@jadwal_kuliah');
    Route::get('/jadwal_kuliah/json','JadwalMahasiswaController@jadwal_kuliah_json');

});

Auth::routes();

// Guard Web
Route::group(['middleware' => ['auth:web']], function () {
    Route::get('/matakuliah/json','MatakuliahController@json');
    Route::get('/dosen/json','DosenController@json');
    Route::get('/validasikrs/{nim}/show','TampilKrsController@show');
    Route::get('/validasikrs/{nim}/selesai','TampilKrsController@selesai');
    Route::get('/validasikrs/mahasiswa_json','TampilKrsController@mahasiswa_json');
    Route::get('/kelompok/json','KelompokController@json');
    Route::get('/jurusan/json','JurusanController@json');
    Route::get('/kelas/json','KelasController@json');
    Route::post('/kelas/hapuskelas','KelasController@hapuskelas');
    Route::get('/validasikrs/json','TampilKrsController@validasi_json');
    Route::get('validasikrs/selesai','TampilKrsController@validasi_index');
    Route::get('/ruangan/json','RuanganController@json');
    Route::get('/mahasiswa/json','MahasiswaController@json');
    Route::get('/tahunakademik/json','TahunakademikController@json');
    Route::get('/dosen/excel','DosenController@exportExcel');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('ajax/kuliah/search','MatakuliahController@ajaxSearch');
    Route::resource('/dosen','DosenController');
    // Route::get('/dosen/{nidn}/show','DosenController@show');
    Route::get('ajax/datadosen/search','DosenController@ajaxSearch');
    Route::get('/mahasiswa/import', 'MahasiswaController@importIndex');
    Route::get('/mahasiswa/cetak','mahasiswaController@mhspdf');
    Route::get('/mahasiswa/semua','mahasiswaController@downPdf');
    Route::get('/mahasiswa/pdf', 'MahasiswaController@pdf');
    Route::post('mahasiswa/import', 'MahasiswaController@storeData');
    Route::get('mahasiswa/download','MahasiswaController@download');


    Route::resource('/matakuliah','MatakuliahController');
    Route::resource('/jadwalkuliah','JadwalkuliahController');
    Route::resource('/validasikrs','TampilKrsController');
    Route::resource('/kelompok','KelompokController');
    Route::resource('/jurusan','JurusanController');
    Route::resource('/kelas','KelasController');
    Route::resource('/ruangan','RuanganController');
    Route::resource('/tahunakademik','TahunakademikController');
    Route::resource('/kurikulum','KurikulumController');
    Route::resource('/mahasiswa','MahasiswaController');
    Route::get('/setting','SettingController@index');
    Route::put('/setting','SettingController@update');
    Route::get('/settingadmin','SettingAdminController@index');
    Route::put('/settingadmin','SettingAdminController@update');

    Route::post('importExcel', 'MahasiswaController@importExcel');

});


Route::get('/tools/sms','ToolsController@sms');
Route::get('/tools/whatapps','ToolsController@whatapps');
